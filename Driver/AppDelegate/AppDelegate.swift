//
//  AppDelegate.swift
//  Driver
//
//  Created by Nirav Shah on 6/22/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import GoogleMaps
import ReachabilitySwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navigationController : UINavigationController?
    
    var objUserLocal : UserLocal!
    var objRideRequestDetail : RideRequestDetail!
    var dictRideDetail : [String : Any]!
    var strRideID : String!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // statusBarStyle
        UIApplication.shared.statusBarStyle = .lightContent
        
        // IQKeyboardManager
        IQKeyboardManager.sharedManager().enable = true
        
        // configure GoogleMapsAPI
        GMSServices.provideAPIKey(Constant.GoogleAPIKey.APIKey)

        // configure fire base
        FirebaseApp.configure()
        
        // set root view controller
        self.onSetRootViewController()

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.

        // Stops monitoring network reachability status changes
        ReachabilityManager.shared.stopMonitoring()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

        // Starts monitoring network reachability status changes
        ReachabilityManager.shared.startMonitoring()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // MARK: - Public Methods

    func onSetRootViewController() {
        
        var storyDashboard = UIStoryboard()
        
        if Auth.auth().currentUser != nil && DataModel.sharedInstance.onCheckIsUserLogIn() {
            storyDashboard = UIStoryboard.init(name: "Main", bundle: nil)
        }
        else {
            storyDashboard = UIStoryboard.init(name: "Authentication", bundle: nil)
        }
        
        let rootVC : UINavigationController = storyDashboard.instantiateInitialViewController() as! UINavigationController
        self.navigationController = rootVC
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.isTranslucent = false
        self.window?.rootViewController = rootVC
        self.window?.makeKeyAndVisible()
        
    }

    func onShowAndHideNavigationBar(isHidden: Bool) {
        self.navigationController?.navigationBar.isHidden = isHidden
    }

    func onFetchUserLocation() {
        // fetch user location
        LocationTracker.sharedInstance.startLocationTracking()
    }

}

