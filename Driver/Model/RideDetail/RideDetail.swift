//
//  RideDetail.swift
//  Driver
//
//  Created by Dhruvit on 31/07/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit
import FirebaseAuth

class RideDetail: NSObject {
    
    var ride_id:String! = EMPTY_STRING
    var customer_id:String! = EMPTY_STRING
    var destination_address:String! = EMPTY_STRING
    var destination_lang:Double! = 0
    var destination_latg:Double! = 0
    var discount:String! = EMPTY_STRING
    var driver_id:String = EMPTY_STRING
    var drop_time:String! = EMPTY_STRING
    var fare:String! = EMPTY_STRING
    var distance:String! = EMPTY_STRING
    var payment_type:Double = 0
    var pickup_time:String! = EMPTY_STRING
    var priceperkm:String! = EMPTY_STRING
    var ride_confirm_time:String! = EMPTY_STRING
    var source_address:String = EMPTY_STRING
    var source_lang:Double! = 0
    var source_latg:Double! = 0
    var tax:String! = EMPTY_STRING
    var timestamp:Double! = 0

    override init() {
        super.init()
    }
    
    init(pdictResponse:[String:Any]) {
        ride_id = pdictResponse[KEY_RIDE_ID]  as? String  ?? EMPTY_STRING
        customer_id = pdictResponse[KEY_CUSTOMER_ID]  as? String  ?? EMPTY_STRING
        driver_id = pdictResponse[KEY_DRIVER_ID]  as? String  ?? EMPTY_STRING
        destination_address = pdictResponse[KEY_DESTINATION_ADDRESS]  as? String  ?? EMPTY_STRING
        destination_latg = pdictResponse[KEY_DESTINATION_LATG]  as? Double  ?? 0
        destination_lang = pdictResponse[KEY_DESTINATION_LANG]  as? Double  ?? 0
        source_address = pdictResponse[KEY_SOURCE_ADDRESS]  as? String  ?? EMPTY_STRING
        source_latg = pdictResponse[KEY_SOURCE_LATG]  as? Double  ?? 0
        source_lang = pdictResponse[KEY_SOURCE_LANG]  as? Double  ?? 0
        discount = pdictResponse[KEY_DISCOUNT]  as? String  ?? EMPTY_STRING
        drop_time = pdictResponse[KEY_DROP_TIME]  as? String  ?? EMPTY_STRING
        fare = pdictResponse[KEY_FARE]  as? String  ?? EMPTY_STRING
        distance = pdictResponse[KEY_DISTANCE]  as? String  ?? EMPTY_STRING
        payment_type = pdictResponse[KEY_PAYMENT_TYPE]  as? Double  ?? 0
        pickup_time = pdictResponse[KEY_PICKUP_TIME]  as? String  ?? EMPTY_STRING
        priceperkm = pdictResponse[KEY_PRICE_PER_KM]  as? String  ?? EMPTY_STRING
        ride_confirm_time = pdictResponse[KEY_RIDE_CONFIRM_TIME]  as? String  ?? EMPTY_STRING
        tax = pdictResponse[KEY_TAX]  as? String  ?? EMPTY_STRING
        timestamp = pdictResponse[KEY_TIMESTAMP] as? Double ?? 0
    }
    
    // add ride detail
    static func onAddRideDetail(){
        let user_id = Auth.auth().currentUser?.uid
        
        FirebaseHelper.sharedInstance.onAddRideDetail(userID: user_id!)
    }
    
    // update ride detail
    static func onUpdateRideDetail(dictDetail : [String:Any]) {
        let user_id = Auth.auth().currentUser?.uid
        
        FirebaseHelper.sharedInstance.onUpdateRideDetail(userID: user_id!, dictDetail: dictDetail)
    }
    
    // MARK: - Get Ride History
    static func onFetchRideHistory(startValue: Double, limitValue: Int,complition : @escaping APICompletionBlock) {
        let user_id = Auth.auth().currentUser?.uid
        
        FirebaseHelper.sharedInstance.onFetchRideHistory(userID: user_id!, startValue: startValue, limitValue: limitValue) { (success, response) in
            complition(success,response)
        }
    }
    
    // MARK: - Get Ride History
    static func onFetchCustomerDetail(customerID : String ,complition : @escaping APICompletionBlock) {
        FirebaseHelper.sharedInstance.onFetchCustomerDetail(customerID: customerID) { (success, response) in
            complition(success,response)
        }
    }
}


class RideDetailList: NSObject {
    
    var ride_detail_list:[RideDetail]! = []
    
    override init() {
        
    }
    
    init(pdictResponse:[String:AnyObject]){
        for (_, value) in pdictResponse{
            print(value)
            ride_detail_list.append(RideDetail.init(pdictResponse: value as! [String : AnyObject]))
        }
    }
}
