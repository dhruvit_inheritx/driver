//
//  RideRequestDetail.swift
//  Driver
//
//  Created by Dhruvit on 10/07/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import Foundation
import FirebaseAuth

class RideRequestDetail : NSObject {
    
    var customerid: String! = ""
    var sourcePlaceName: String! = ""
    var destPlacename: String! = ""
    var isPendingRequest: Int!
    var name: String! = ""
    var mobile: String! = ""
    var nameValuePairs: String! = ""
    var destLat: Double! = 0
    var destLong: Double! = 0
    var currentLat: Double! = 0
    var currentLong: Double! = 0
    var sourceLat: Double! = 0
    var sourceLong: Double! = 0
    
    override init() {
        super.init()
    }
    
    // init with dictionary
    init(dictionary : [String : Any]) {
        
        self.customerid = dictionary["customerid"] as? String
        self.sourcePlaceName = dictionary["sourcePlaceName"] as? String
        self.destPlacename = dictionary["destPlacename"] as? String
        self.isPendingRequest = dictionary["isPendingRequest"] as? Int
        self.name = (dictionary["name"] as? String?)!
        self.mobile = (dictionary["mobile"] as? String?)!
        self.nameValuePairs = (dictionary["nameValuePairs"] as? String?)!
        
        self.destLat = dictionary["destLat"] as? Double
        self.destLong = dictionary["destLong"] as? Double
        self.currentLat = dictionary["currentLat"] as? Double
        self.currentLong = (dictionary["currentLong"] as? Double?)!
        self.sourceLat = (dictionary["sourceLat"] as? Double?)!
        self.sourceLong = (dictionary["sourceLong"] as? Double?)!
    }
    
    // update ride status
    static func onUpdateRideStatus(isPendingRequest : RideRequestStatus) {
        let userID = Auth.auth().currentUser?.uid
        
        FirebaseHelper.sharedInstance.onUpdateRideStatus(isPendingRequest: isPendingRequest, userID: userID!)
    }
    
    // observer for ride status change
    static func onAddObserverForRideStatusChange() {
        let userID = Auth.auth().currentUser?.uid
        
        FirebaseHelper.sharedInstance.onAddObserverForRideStatus(userID: userID!)
    }
    
}
