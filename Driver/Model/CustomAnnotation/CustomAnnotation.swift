//
//  CustomAnnotation.swift
//  Driver
//
//  Created by Nirav Shah on 6/27/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import Foundation
import MapKit
import GoogleMaps

class CustomAnnotation: GMSMarker {

    var index : Int = 0
    
    // init with title, locationName, coordinate
    init(title: String, locationName: String, coordinate: CLLocationCoordinate2D) {
        super.init()
        self.title = title
        self.snippet = locationName
        self.position = coordinate
    }
    
    // init with title, locationName
    init(title: String, locationName: String) {
        super.init()
        self.title = title
        self.snippet = locationName
    }
}
