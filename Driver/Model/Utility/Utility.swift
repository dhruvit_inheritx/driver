//
//  Utility.swift
//  Driver
//
//  Created by Nirav Shah on 6/23/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import GoogleMaps

class Utility: NSObject {

    static let sharedInstance = Utility()

    private override init() {
        // restrict to use initalisation of object
    }
    
    // get Date difference for Date
    func getDateDifferenceforDate(_ pobjFirstDate: Date, andDate pobjLastDate: Date) -> String {
        
        var strTime: String
        let distanceBetweenDates: TimeInterval = pobjLastDate.timeIntervalSince(pobjFirstDate)
        let secondsInAnHour: Double = 3600
        let daysBetweenDates: Int = Int((distanceBetweenDates / secondsInAnHour) / 24)
        let weekBetweenDate:Int = Int((distanceBetweenDates / secondsInAnHour) / (24*7))
        
        if weekBetweenDate > 48{
            strTime = "\(Int(weekBetweenDate)/4/12) \(STRYEARAGO)"
            return strTime
        }
        if weekBetweenDate >= 4{
            strTime = "\(Int(weekBetweenDate)/4) \(STRMONTHAGO)"
            return strTime
        }
        if weekBetweenDate > 0{
            strTime = "\(Int(weekBetweenDate)) \(STRWEEKAGO)"
            return strTime
        }
        
        if daysBetweenDates > 0  && daysBetweenDates <= 1 {
            strTime = "\(Int(daysBetweenDates)) \(STRDAYAGO)"
            return strTime
        }
        else if daysBetweenDates > 0 {
            strTime = "\(Int(daysBetweenDates)) \(STRDAYSAGO)"
            return strTime
        }
        
        let hoursBetweenDates: Int = Int((distanceBetweenDates / secondsInAnHour))
        if hoursBetweenDates > 0 && hoursBetweenDates <= 1 {
            strTime = "\(Int(hoursBetweenDates)) \(STRHOURAGO)"
            return strTime
        }
        else if hoursBetweenDates > 1 {
            strTime = "\(Int(hoursBetweenDates)) \(STRHOURSAGO)"
            return strTime
        }
        
        let minutesBetweenDates: Int = Int((distanceBetweenDates / secondsInAnHour) * 60)
        if minutesBetweenDates > 0 && minutesBetweenDates <= 1{
            strTime = "\(Int(minutesBetweenDates)) \(STRMINUTEAGO)"
            return strTime
        }
        else if minutesBetweenDates > 0 {
            strTime = "\(Int(minutesBetweenDates)) \(STRMINUTESAGO)"
            return strTime
        }
        
        if minutesBetweenDates == 0 {
            strTime = STRFEWSECONDSAGO
            return strTime
        }
        
        return STREMPTY
    }
    
    // calculate distance between two cordinates
    func oncalculateDistanceBetween(source: CLLocationCoordinate2D, destination: CLLocationCoordinate2D, complitionHandler: @escaping (Double) -> () ) {
        
        self.onFetchPathCoordinatesArray(source: source, destination: destination) { (arrayCoordinate) in
            
            self.onCalculateDistanceFrom(arrayCoordinate: arrayCoordinate, complition: { (distance) in
                complitionHandler(distance)
            })
        }
    }
    
    func onCalculateDistanceFrom(arrayCoordinate:[CLLocationCoordinate2D], complition: (Double) -> () ) {
        // find Distance between two location
        var coordinateFirst = arrayCoordinate.first
        var totalMeters = 0.0
        for coordinateTemp in arrayCoordinate {
            let distanceFound = Utility.sharedInstance.onFindDistanceInMeterBetween(toCoordinate: coordinateFirst!, fromCoordinate: coordinateTemp)
            coordinateFirst = coordinateTemp
            totalMeters += distanceFound
        }
        complition(totalMeters)
    }
    
    // fetch path cordinates array
    func onFetchPathCoordinatesArray(source: CLLocationCoordinate2D, destination: CLLocationCoordinate2D, complitionHandler: @escaping ([CLLocationCoordinate2D]) -> () ) {
        
        let origin = "\(source.latitude),\(source.longitude)"
        let destination = "\(destination.latitude),\(destination.longitude)"
        
        let strUrl = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(Constant.GoogleAPIKey.APIKey)"
        
        let urlPath = URL.init(string: strUrl)
        let urlRequest = URLRequest.init(url: urlPath!)
        
        let dataTask = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error == nil {
                
                do {
                    let jsonDict = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! NSDictionary
                    
                    let routesArray = jsonDict["routes"] as! NSArray
                    
                    if ((routesArray as AnyObject).count)! > 0 {
                        var routeDict: [AnyHashable: Any]? = (routesArray[0] as? [AnyHashable: Any])
                        var routeOverviewPolyline: [AnyHashable: Any]? = (routeDict?["overview_polyline"] as? [AnyHashable: Any])
                        let points: String? = (routeOverviewPolyline?["points"] as? String)
                        let path = GMSPath.init(fromEncodedPath: points!)
                        
                        var coordinateArray = [CLLocationCoordinate2D]()
                        
                        var intTemp = 0
                        while UInt(intTemp) < (path?.count())! {
                            coordinateArray.append((path?.coordinate(at: UInt(intTemp)))!)
                            intTemp += 1
                        }
                        DispatchQueue.main.async(execute: {
                            complitionHandler(coordinateArray)
                        })
                    }
                }
                catch _ {
                    print("Json failure")
                }
            }
        }
        
        dataTask.resume()
    }
    
    // get unique random string
    func random(length: Int = 20) -> String {
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""
        
        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.characters.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        let timeStamp: TimeInterval = Date().timeIntervalSince1970
        let timeStampString : String = "\(timeStamp)"
//        timeStampString = timeStampString.replacingOccurrences(of: ".", with: "")
//        return randomString + timeStampString
        let arrayString = timeStampString.components(separatedBy: ".")
        return randomString + arrayString[0]
    }
    
//    func getCurrentTimeStamp() -> String {
//        let timeStamp: TimeInterval = Date().timeIntervalSince1970
//        var timeStampString: String = "\(timeStamp)"
//        timeStampString = timeStampString.replacingOccurrences(of: ".", with: "")
//        return timeStampString
//    }
    
    func getCurrentTimeStamp() -> String {
        let timeStamp: TimeInterval = Date().timeIntervalSince1970
        let timeStampString: String = "\(timeStamp)"
        let arrayString = timeStampString.components(separatedBy: ".")
        return arrayString[0]
    }
    
    // get google map string for navigation
    func getGoogleMapString(saddrLat:String!, saddrLong:String!, daddrLat:String!, daddrLong:String!) -> String {
        
        let strPath = "comgooglemaps://?saddr=\(saddrLat!),\(saddrLong!)&daddr=\(daddrLat!),\(daddrLong!)&zoom=14&views=traffic"
        return strPath
    }
    
    // MARK: - Validation Methods
    
    // check empty string
    func isStringEmpty(string : String?) -> Bool {
        var returnValue = true

        if string != nil && string?.isEmpty == false && string?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == false {
            returnValue = false
        }

        return returnValue
    }
    
    // email id validation
    func isValidEmail(email : String?) -> Bool {

        var returnValue = false

        if email != nil {
            let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)

            returnValue = emailTest.evaluate(with: email)
        }

        return returnValue
    }
    
    // phone number validation
    func isValidPhoneNumber(phoneNumber : String) -> Bool {

        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"

        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: phoneNumber)

        return result
    }
    
    func isValidPhone(strPhone:String) -> Bool {
        return strPhone.removeSpecialCharsFromPhoneNumberString(str: strPhone).characters.count == MaxCharacter.Phonenum
    }
    
    // password validation
    func isPasswordValid(password : String?) -> Bool {
        var returnValue = false

        if password != nil {

            //            let capitalLetterRegEx  = ".*[A-Z]+.*"
            //            let predictCapital = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
            //            let capitalresult = predictCapital.evaluate(with: password)
            //            print("\(capitalresult)")
            //
            //            let smallLetterRegEx  = ".*[a-z]+.*"
            //            let predictSmall = NSPredicate(format:"SELF MATCHES %@", smallLetterRegEx)
            //            let smallResult = predictSmall.evaluate(with: password)
            //            print("\(smallResult)")
            //
            //            let numberRegEx  = ".*[0-9]+.*"
            //            let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
            //            let numberresult = texttest1.evaluate(with: password)
            //            print("\(numberresult)")
            //
            //
            //            let specialCharacterRegEx  = ".*[!&^%$#@()/]+.*"
            //            let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
            //
            //            let specialresult = texttest2.evaluate(with: password)
            //            print("\(specialresult)")

            let finalRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,}"
            let predictFinal = NSPredicate(format:"SELF MATCHES %@", finalRegex)

            returnValue = predictFinal.evaluate(with: password)
        }

        return returnValue
    }

    // MARK: - Degress Methods
    
    /// Returns the amount of minutes from another date
    func minutes(from date: Date, toDate: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: toDate).minute ?? 0
    }
    
    // Find Distance In Meter Between two coordinates
    func onFindDistanceInMeterBetween(toCoordinate coordinate : CLLocationCoordinate2D, fromCoordinate : CLLocationCoordinate2D) -> CLLocationDistance {
        let sourceLocation = CLLocation(latitude: (coordinate.latitude), longitude: (coordinate.longitude))
        let destinLocation = CLLocation(latitude: (fromCoordinate.latitude), longitude: (fromCoordinate.longitude))

        return sourceLocation.distance(from: destinLocation)
    }
    
    // degreesToRadians
    func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
    
    // radiansToDegrees
    func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }
    
    // getBearing using two location
    func getBearing(toPoint point: CLLocationCoordinate2D,fromPoint pointFrom: CLLocationCoordinate2D) -> Double {

        let lat1 = self.degreesToRadians(degrees: pointFrom.latitude)
        let lon1 = self.degreesToRadians(degrees: pointFrom.longitude)

        let lat2 = self.degreesToRadians(degrees: point.latitude);
        let lon2 = self.degreesToRadians(degrees: point.longitude);

        let dLon = lon2 - lon1;

        let y = sin(dLon) * cos(lat2);
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
        var radiansBearing = atan2(y, x);

        if radiansBearing < 0.0 {
            radiansBearing += (2 * .pi)
        }

//        let degrees = radiansToDegrees(radians: radiansBearing)
//
//        let ret = (degrees + 360) .truncatingRemainder(dividingBy: <#T##(Double)#>)
//        
//        return ret;

        return radiansToDegrees(radians: radiansBearing)
    }
}
