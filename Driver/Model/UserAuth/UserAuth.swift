//
//  UserAuth.swift
//  Driver
//
//  Created by Nirav Shah on 6/26/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit
import FirebaseAuth

class UserAuth: NSObject {
    
    // Signin user
    static func onSiginUser(withEmail: String, password: String, completion: @escaping (_ user: Any?,_ error: Error?) -> ()) {
        Auth.auth().signIn(withEmail: withEmail, password: password) { (user, error) in
            completion(user,error)
        }
    }
    
    // create user
    static func onCreateUser(withEmail: String, password: String, userLocal: UserLocal, completion: @escaping(_ user: Any?, _ error : Error?) -> ()) {
        
        Auth.auth().createUser(withEmail: withEmail, password: password) { (user, error) in
            
            let userID = Auth.auth().currentUser?.uid
            
            FirebaseHelper.sharedInstance.onAddDriverDetials(userLocal: userLocal, userID: userID!)
            
            completion(user,error)
        }
    }
    
    // save user detail in local
    static func onSaveUserDataInLocal() {
        let userID = Auth.auth().currentUser?.uid
        
        FirebaseHelper.sharedInstance.onSaveUserDataInLocal(userID: userID!)
    }
    
    // delete account
    static func onDeleteDriverAccount(completion: @escaping (_ error : Error?) -> ()) {
        let userID = Auth.auth().currentUser?.uid
        
        FirebaseHelper.sharedInstance.onDeleteDriverAccount(userID: userID!) { (error) in
            if error != nil {
                completion(error)
            }
        }
    }
    
    // update driver location
    static func onUpdateDriverLocation(latitude: Double?, longitude: Double?) {
        let userID = Auth.auth().currentUser?.uid

        FirebaseHelper.sharedInstance.onUpdateDriverLocation(userID: userID, latitude: latitude, longitude: longitude)
    }
    
    // update driver availability
    static func onUpdateDriverAvailability(availability : Bool) {
        let userID = Auth.auth().currentUser?.uid

        FirebaseHelper.sharedInstance.onUpdateDriverAvailability(availability: availability, userID: userID!)
    }
    
    // update profile
    static func onUpdateDriverProfileDetail(dictDetail: [String:Any]) {
        let userID = Auth.auth().currentUser?.uid
        
        FirebaseHelper.sharedInstance.onUpdateDriverProfileDetail(userID: userID!, dictDetail: dictDetail)
    }
    
    // change password
    static func onUpdatePassword(password:String,completion:@escaping (_ error:Error?) -> ()) {
        FirebaseHelper.sharedInstance.onUpdatePassword(password: password) { (error) in
            completion(error)
        }
    }
    
    // observer for driver availability change
    static func onAddObserverForAvailabilityChange() {
        let userID = Auth.auth().currentUser?.uid

        FirebaseHelper.sharedInstance.onAddObserverForUserRequest(userID: userID!)
    }
    
    // check user is driver
    static func onCheckIsUserDriver(completion: @escaping (_ status : Bool) -> ()) {

        let userID = Auth.auth().currentUser?.uid

        FirebaseHelper.sharedInstance.onCheckIsUserDriver(userID: userID!) { (value) in
            completion(value)
        }
    }
}
