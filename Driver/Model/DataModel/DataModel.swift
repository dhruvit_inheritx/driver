import UIKit

let KEY_USER_LOGIN                  = "islogin"
let KEY_USER_NAME                   = "username"
let KEY_USER_FULLNAME               = "fullname"
let KEY_USER_EMAIL                  = "email"
let KEY_USER_MOBILE                  = "mobile"

class DataModel: NSObject {

    static let sharedInstance = DataModel()
    
    // Check Is User LogIn
    func onCheckIsUserLogIn() -> Bool {
        let isLogged = UserDefaults.standard.bool(forKey: KEY_USER_LOGIN)
        return isLogged
    }
    
    // Set User LogIn
    func onSetUserLogIn(value : Bool) {
        UserDefaults.standard.set(value, forKey: KEY_USER_LOGIN)
        UserDefaults.standard.synchronize()
    }
    
    // Fetch User Name
    func onFetchUserName() -> String? {
        let userName : String? = UserDefaults.standard.object(forKey: KEY_USER_NAME) as? String
        return userName
    }
    
    // Set User Name
    func onSetUserName(userName : String?) {
        UserDefaults.standard.setValue(userName, forKey: KEY_USER_NAME)
        UserDefaults.standard.synchronize()
    }
    
    // Fetch User Email Address
    func onFetchUserEmailAddress() -> String? {
        let userEmailAddress : String? = UserDefaults.standard.object(forKey: KEY_USER_EMAIL) as? String
        return userEmailAddress
    }
    
    // Set User Email Address
    func onSetUserEmailAddress(userEmailAddress : String?) {
        UserDefaults.standard.setValue(userEmailAddress, forKey: KEY_USER_EMAIL)
        UserDefaults.standard.synchronize()
    }
    
    // Fetch User Full Name
    func onFetchUserFullName() -> String? {
        let userFullName : String? = UserDefaults.standard.object(forKey: KEY_USER_FULLNAME) as? String
        return userFullName
    }
    
    // Set User Full Name
    func onSetUserFullName(userFullName : String?) {
        UserDefaults.standard.setValue(userFullName, forKey: KEY_USER_FULLNAME)
        UserDefaults.standard.synchronize()
    }

}
