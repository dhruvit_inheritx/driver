//
//  FirebaseHelper.swift
//  Driver
//
//  Created by Nirav Shah on 6/26/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

typealias APICompletionBlock = (Bool,Any?) -> ()

class FirebaseHelper: NSObject {
    
    static let sharedInstance = FirebaseHelper()
    
    // Firebase Database Reference
    var reference: DatabaseReference {
        get {
            return Database.database().reference()
        }
    }
    
    override init() {
        super.init()
    }
    
    // MARK: - Handle Driver Details
    
    // delete driver account
    func onDeleteDriverAccount(userID : String,completion: @escaping (_ error : Error?) -> ()) {
        Auth.auth().currentUser?.delete(completion: { (error) in
            if error != nil {
                completion(error)
            }
        })
    }
    
    // check user is driver
    func onCheckIsUserDriver(userID : String, completion: @escaping (_ value : Bool) -> ()) {
        self.reference.child("\(KEY_DRIVER_DETAILS)/\(userID)").observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            let postDict = snapshot.value as? NSDictionary
            print("PostDict : \(String(describing: postDict))")
            
            if postDict != nil {
                completion(true)
            }
            else {
                completion(false)
            }
        })
    }
    
    // add driver detail
    func onAddDriverDetials(userLocal: UserLocal?, userID : String) {
        self.onAddDriverDetails(userID: userID, firstName: (userLocal?.userFirstName!)!, lastName: (userLocal?.userLastName!)!, mobile: (userLocal?.userPhoneNumber!)!, email: (userLocal?.userEmail!)!, latitude: userLocal?.latitude, longitude: userLocal?.longitude)
    }
    
    private func onAddDriverDetails(userID : String, firstName: String, lastName: String, mobile: String, email: String, latitude: Double?, longitude: Double?) {
        var dictLocation : Dictionary = [KEY_LATITUDE : 0.0,KEY_LONGITUDE : 0.0,"bearing":0.0,"accurancy":0.0]

        if let tempLat = latitude {
            dictLocation[KEY_LATITUDE] = tempLat
        }

        if let tempLong = longitude {
            dictLocation[KEY_LONGITUDE] = tempLong
        }

        let jsonData: NSData
        var jsonString = String()

        do {
            jsonData = try JSONSerialization.data(withJSONObject: dictLocation, options: JSONSerialization.WritingOptions()) as NSData
            let jsonInnerString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
//            print("json string = \(jsonInnerString)")
            
            jsonString = jsonInnerString

        } catch _ {
            print ("JSON Failure")
        }

        // set user records
        let strSeenDate : String = String(format: "%@", Date() as CVarArg)
        let dictUser : Dictionary = [KEY_USER_ID : userID, KEY_EMAIL : email,KEY_AVAILABLE : true, KEY_MOBILE : mobile,KEY_AGE : 30, KEY_CITY : "ahmedabad",KEY_STATE : "gujarat",KEY_LAST_SEEN : strSeenDate,KEY_FIRSTNAME : firstName,KEY_LASTNAME : lastName,KEY_VECHICAL_DETAIL : "", KEY_LOCATION : jsonString] as [String : Any]

//        let dictMain : Dictionary = [userID:dictUser]

        // add user record in driver details
        self.reference.child(KEY_DRIVER_DETAILS).child(userID).setValue(dictUser)
    }
    
    // update driver location
    func onUpdateDriverLocation(userID : String?, latitude: Double?, longitude: Double?) {
        var dictLocation : Dictionary = [KEY_LATITUDE : 0.0,KEY_LONGITUDE : 0.0,"bearing":0.0,"accurancy":0.0]

        if let tempLat = latitude {
            dictLocation[KEY_LATITUDE] = tempLat
        }

        if let tempLong = longitude {
            dictLocation[KEY_LONGITUDE] = tempLong
        }

        let jsonData: NSData
        var jsonString = String()

        do {
            jsonData = try JSONSerialization.data(withJSONObject: dictLocation, options: JSONSerialization.WritingOptions()) as NSData
            let jsonInnerString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
//            print("json string = \(jsonInnerString)")
            
            jsonString = jsonInnerString

        } catch _ {
            print ("JSON Failure")
        }

        // update driver location

        let childUpdates = ["\(KEY_DRIVER_DETAILS)/\(userID!)/\(KEY_LOCATION)" : jsonString]
        self.reference.updateChildValues(childUpdates)
    }
    
    // update driver availability
    func onUpdateDriverAvailability(availability : Bool ,userID : String)  {
        let childUpdates = ["\(KEY_DRIVER_DETAILS)/\(userID)/available" : availability] as [String : Any]
        self.reference.updateChildValues(childUpdates)
        if (APPDELEGATE.objUserLocal) != nil {
            APPDELEGATE.objUserLocal.isDriverAvailable = availability
        }
        else {
            UserAuth.onSaveUserDataInLocal()
        }
    }
    
    // update driver profile detail
    func onUpdateDriverProfileDetail(userID : String, dictDetail : [String:Any]) {
        
        for (key, value) in dictDetail {
            let childUpdates = ["\(KEY_DRIVER_DETAILS)/\(userID)/\(key)" : value]
            self.reference.updateChildValues(childUpdates)
        }
        
        UserAuth.onSaveUserDataInLocal()
    }
    
    // update password
    func onUpdatePassword(password:String, completion:@escaping (_ error: Error?) -> ()){
        
        Auth.auth().signIn(withEmail: APPDELEGATE.objUserLocal.userEmail!, password: password) { (user, error) in
            if (error != nil) {
                Auth.auth().currentUser?.updatePassword(to: password, completion: { (error) in
                    completion(error)
                })
            }
            else {
                completion(error)
            }
        }
    }
    
    // Save User Detail in Local
    func onSaveUserDataInLocal(userID : String) {
        self.reference.child(KEY_DRIVER_DETAILS).child(userID).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            
            if let _ : NSNull = snapshot.value as? NSNull {
                return
            }
            
            let userDetail = snapshot.value
            print("userDetail : \(String(describing: userDetail!))")
            UserDefaults.standard.setValue(userDetail, forKey: KEY_USERDATA)
            APPDELEGATE.objUserLocal = UserLocal(dictionary: userDetail as! [String : Any])
        })
    }
    
    // MARK: - Handle Ride Request
    
    // observer for user request
    func onAddObserverForUserRequest(userID : String) {
        self.reference.child("\(KEY_DRIVER_DETAILS)/\(userID)/available").observe(DataEventType.value, with: { (snapshot) in
            let postDict = snapshot.value
            print("Check Availability : \(String(describing: postDict!))")
        })
    }
    
    // update ride status
    func onUpdateRideStatus(isPendingRequest : RideRequestStatus ,userID : String)  {
        let objRideDetail : RideRequestDetail! = APPDELEGATE.objRideRequestDetail
        var dictRideRequest : Dictionary =  ["customerid":objRideDetail.customerid,"sourcePlaceName":objRideDetail.sourcePlaceName!, "isPendingRequest": isPendingRequest.rawValue, "name": objRideDetail.name, "mobile": objRideDetail.mobile, "nameValuePairs": objRideDetail.nameValuePairs, "destLong": objRideDetail.destLong, "currentLat": objRideDetail.currentLat, "sourceLat": objRideDetail.sourceLat, "currentLong": objRideDetail.currentLong, "destLat": objRideDetail.destLat, "destPlacename": objRideDetail.destPlacename!, "sourceLong": objRideDetail.sourceLong] as [String : Any]
        
        // Change Ride Status
        dictRideRequest["isPendingRequest"] = isPendingRequest.rawValue
        
        let jsonData: NSData
        var jsonString = String()
        
        do {
            jsonData = try JSONSerialization.data(withJSONObject: dictRideRequest, options: JSONSerialization.WritingOptions()) as NSData
            let jsonInnerString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
//            print("json string = \(jsonInnerString)")
            
            jsonString = jsonInnerString
            
        } catch _ {
            print ("JSON Failure")
        }
        
        // update driver location
        
        let childUpdates = ["\(KEY_RIDE_NOTIFICATION)/\(userID)/customer_detail" : jsonString]
        self.reference.updateChildValues(childUpdates)
    }
    
    // observer for ride request
    func onAddObserverForRideStatus(userID : String) {
        self.reference.child("\(KEY_RIDE_NOTIFICATION)/\(userID)/customer_detail").observe(DataEventType.value, with: { (snapshot) in
            
            if let _ : NSNull = snapshot.value as? NSNull {
                return
            }
            
            let postDict = snapshot.value as! String
            print("Change ride_status : \(String(describing: postDict))")
            
            let dictResponse : [String:Any]!
            if let data = postDict.data(using: .utf8) {
                do {
                    dictResponse = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                    print("dictResponse :- ",dictResponse["isPendingRequest"]!)
                    
                    APPDELEGATE.objRideRequestDetail = RideRequestDetail(dictionary: dictResponse)
                    
                    let isPendingRequest : Int = dictResponse["isPendingRequest"]! as! Int
                    
                    if isPendingRequest == RideRequestStatus.INITIAL.rawValue {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kRideRequestInitialStateKey), object: nil)
                    }
                    
                    if isPendingRequest == RideRequestStatus.CONFIRM.rawValue {
                        
                        RideDetail.onAddRideDetail()
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kRideRequestConfirmationStateKey), object: nil)
                    }
                }
                catch {
                    print(error.localizedDescription)
                }
            }
        })
    }
    
    // MARK: - Add / Update Ride Detail
    
    // Add ride detail
    func onAddRideDetail(userID:String) {
        
        // Generate ride id
        let randomString = Utility.sharedInstance.random()
        APPDELEGATE.strRideID = randomString
        
        // Generate ride confirm time
        let strRideConfirmTime : String = String(format: "%@", Date() as CVarArg)
        let strTimeStamp : String = Utility.sharedInstance.getCurrentTimeStamp()
        
        // set ride detail
        let objRideReq = APPDELEGATE.objRideRequestDetail
        let dictRideDetails : Dictionary
            = [KEY_RIDE_ID : randomString,
               KEY_CUSTOMER_ID :objRideReq!.customerid!,
               KEY_DRIVER_ID : userID,
               KEY_CUSTOMERID_TIMESTAMP : "\(objRideReq!.customerid!)_\(strTimeStamp)",
                KEY_DRIVERID_TIMESTAMP : "\(userID)_\(strTimeStamp)",
                KEY_SOURCE_ADDRESS : objRideReq!.sourcePlaceName!,
                KEY_DESTINATION_ADDRESS : objRideReq!.destPlacename!,
                KEY_SOURCE_LATG : objRideReq!.sourceLat!,
                KEY_SOURCE_LANG : objRideReq!.sourceLong!,
                KEY_DESTINATION_LATG : objRideReq!.destLat!,
                KEY_DESTINATION_LANG : objRideReq!.destLong!,
                KEY_FARE : 0.getAmountInRupees(),
                KEY_DISTANCE : 0.getDistanceInKM(),
                KEY_PRICE_PER_KM : ONE_KM_AMOUNT.getAmountInRupees(),
                KEY_DISCOUNT : DISCOUNT_PERCENTAGE.getPercentage(),
                KEY_TAX : TAX_PERCENTAGE.getPercentage(),
                KEY_RIDE_CONFIRM_TIME : strRideConfirmTime,
                KEY_PICKUP_TIME : "",
                KEY_DROP_TIME : "",
                KEY_PAYMENT_TYPE : EMPTY_STRING,
                KEY_TIMESTAMP : Int(strTimeStamp)!] as [String : Any]
        
        APPDELEGATE.dictRideDetail = dictRideDetails
        
        // add ride detail in ride details
        self.reference.child(KEY_RIDE_DETAILS).child(randomString).setValue(dictRideDetails)
    }
    
    // update ride detail
    func onUpdateRideDetail(userID : String, dictDetail : [String:Any]) {
        
        for (key, value) in dictDetail {
            let childUpdates = ["ride_details/\(APPDELEGATE.strRideID!)/\(key)" : value]
            self.reference.updateChildValues(childUpdates)
        }
    }
    
    // MARK: - Get Ride History
    
    // fetch ride history
    func onFetchRideHistory(userID : String, startValue:Double, limitValue:Int, complition : @escaping APICompletionBlock) {
        
        if startValue == 0 {
            
            self.reference.child(KEY_RIDE_DETAILS).queryOrdered(byChild: "driverid_timestamp").queryStarting(atValue: userID + "_").queryLimited(toFirst: UInt(limitValue)).observeSingleEvent(of: .value, with: { (snapshot) in
                
                if let _ : NSNull = snapshot.value as? NSNull {
                    complition(false,"No Post Found.")
                    return
                }
                
                let rideIdDict = snapshot.value as! [String:Any]
                
                var arrayDict = rideIdDict.getAllValues() as! [Dictionary<String, Any>]
                arrayDict = arrayDict.sorted(by: {
                    (($0)[KEY_TIMESTAMP] as? Int)! < (($1)[KEY_TIMESTAMP] as? Int)!
                })
                
//                print("array ride history 1 :- ",arrayDict as NSArray)
                print("array ride history 1 - Count :- ",arrayDict.count)
                
                var arrayRideDetail = [RideDetail]()
                
                for dict in arrayDict {
                    arrayRideDetail.append(RideDetail(pdictResponse: dict))
                }
                
                DispatchQueue.main.async(execute: {
                    complition(true,arrayRideDetail)
                })
            })
            
        }
        else {
            
            let arr = "\(startValue)".components(separatedBy: ".")
            let arr2 = Int(arr[1])!
            let last = "\(Int(arr[0])! + 1).\(arr2)"
            let checkLast = (last as NSString).doubleValue
            
            let string : String = String(format: "%.0f", checkLast)
            
            self.reference.child(KEY_RIDE_DETAILS).queryOrdered(byChild: "driverid_timestamp").queryStarting(atValue: userID+"_"+string).queryEnding(atValue: userID + "_\\uf8ff").queryLimited(toFirst: UInt(limitValue)).observeSingleEvent(of: .value , with: { (snapshot) in
                
                if let _ : NSNull = snapshot.value as? NSNull {
                    complition(false,"No Post Found.")
                    return
                }
                
                let rideIdDict = snapshot.value as! [String:Any]
                
                var arrayDict = rideIdDict.getAllValues() as! [Dictionary<String, Any>]
                arrayDict = arrayDict.sorted(by: {
                    (($0)[KEY_TIMESTAMP] as? Int)! < (($1)[KEY_TIMESTAMP] as? Int)!
                })
                
//                print("array ride history 2 :- ",arrayDict as NSArray)
                print("array ride history 2 - Count :- ",arrayDict.count)
                
                var arrayRideDetail = [RideDetail]()
                
                for dict in arrayDict {
                    arrayRideDetail.append(RideDetail(pdictResponse: dict))
                }
                
                DispatchQueue.main.async(execute: {
                    complition(true,arrayRideDetail)
                })
            })
        }
    }
    
    // fetch customer detail to display in RideDetailVC
    func onFetchCustomerDetail(customerID : String , complition : @escaping APICompletionBlock ) {
        
        self.reference.child("customer_detail").child(customerID).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            
            if let _ : NSNull = snapshot.value as? NSNull {
                complition(false,"User Not Found.")
                return
            }
            
            let dictCustomerDetail = snapshot.value as! [String:Any]
            print("Customer Detail :- ",dictCustomerDetail)
            complition(true,dictCustomerDetail)
        })
        
    }
}
