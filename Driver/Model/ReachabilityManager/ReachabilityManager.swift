//
//  ReachabilityManager.swift
//  Driver
//
//  Created by Nirav Shah on 7/4/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit
import ReachabilitySwift

class ReachabilityManager: NSObject {

    static let shared = ReachabilityManager()
    var reachabilityStatus: Reachability.NetworkStatus = .notReachable
    let reachability = Reachability()!

    var isNetworkAvailable : Bool {
        return reachabilityStatus == .notReachable
    }

    /// Called whenever there is a change in NetworkReachibility Status
    ///
    /// — parameter notification: Notification with the Reachability instance
    func reachabilityChanged(notification: Notification) {
        let reachability = notification.object as! Reachability
        switch reachability.currentReachabilityStatus {
        case .notReachable:
            debugPrint("Network became unreachable")
        case .reachableViaWiFi:
            debugPrint("Network reachable through WiFi")
        case .reachableViaWWAN:
            debugPrint("Network reachable through Cellular Data")
        }
    }

    func startMonitoring() {
        NotificationCenter.default.addObserver(self,selector: #selector(self.reachabilityChanged),
                                               name: ReachabilityChangedNotification,
                                               object: reachability)
        do{
            try reachability.startNotifier()
        } catch {
            debugPrint("Could not start reachability notifier")
        }
    }

    func stopMonitoring(){
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self,
                                                  name: ReachabilityChangedNotification,
                                                  object: reachability)
    }
}
