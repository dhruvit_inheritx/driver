//
//  UserLocal.swift
//  Driver
//
//  Created by Nirav Shah on 6/27/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit

class UserLocal: NSObject {

    var userEmail: String?
    var userPhoneNumber: String?
    var userFirstName: String?
    var userLastName: String?
    var userFullName: String?
    var isDriverAvailable : Bool = false
    var latitude: Double? = 0
    var longitude: Double? = 0
    var userID: String?

    override init() {
        super.init()
    }
    
    // init with dictionary
    init(dictionary : [String : Any]) {
        self.userEmail = dictionary[KEY_EMAIL] as? String
        self.userPhoneNumber = dictionary[KEY_MOBILE] as? String
        self.userFirstName = dictionary[KEY_FIRSTNAME] as? String
        self.userLastName = dictionary[KEY_LASTNAME] as? String
        self.userFullName = self.userFirstName! + " " + self.userLastName!
        self.isDriverAvailable = (dictionary[KEY_AVAILABLE] as? Bool)!
        self.userID = ""
    }
    
    // init with details
    func initalisation(withEmail: String, phoneNumber: String, firstName: String, lastName: String, latitude: Double?, longitude: Double?)  {

        self.userEmail = withEmail
        self.userPhoneNumber = phoneNumber
        self.userFirstName = firstName
        self.userLastName = lastName
        self.userFullName = firstName + " " + lastName
        self.latitude = latitude
        self.longitude = longitude
        self.userID = ""
    }
}
