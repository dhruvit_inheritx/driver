//
//  BackgroundTaskManager.swift
//  Driver
//
//  Created by Nirav Shah on 6/26/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit

class BackgroundTaskManager: NSObject {

    var bgTaskIdList = NSMutableArray()
    var masterTaskId = UIBackgroundTaskIdentifier()

    static let sharedInstance = BackgroundTaskManager()

    // MARK: - Initalisation Method

    override init() {
        super.init()

        bgTaskIdList = NSMutableArray()
        masterTaskId = UIBackgroundTaskInvalid
    }

    // MARK: - Public Methods

    func beginNewBackgroundTask() {
        let application = UIApplication.shared
        var bgTaskId: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid

        if application.responds(to: #selector(application.beginBackgroundTask(expirationHandler:))) {

            bgTaskId = application.beginBackgroundTask(expirationHandler: { 
                print("background task \(UInt(bgTaskId)) expired")

                self.bgTaskIdList.removeObject(at: self.bgTaskIdList.index(of: bgTaskId))

                application.endBackgroundTask(bgTaskId)
                bgTaskId = UIBackgroundTaskInvalid
            })

            if self.masterTaskId == UIBackgroundTaskInvalid {
                self.masterTaskId = bgTaskId
                print("started master task \(UInt(masterTaskId))")
            }
            else {
                // add this id to our list
                print("started background task \(UInt(bgTaskId))")
                self.bgTaskIdList.add(bgTaskId)
                self.endBackgroundTasks()
            }
        }
    }

    func endBackgroundTasks() {
        drainBGTaskList(false)
    }

    func endAllBackgroundTasks() {
        drainBGTaskList(true)
    }

    func drainBGTaskList(_ all: Bool) {
        // mark end of each of our background task
        let application = UIApplication.shared

        if application.responds(to: #selector(self.endBackgroundTasks)) {
            let count: Int = bgTaskIdList.count

            for _ in (all ? 0 : 1)..<count {
                let bgTaskId: UIBackgroundTaskIdentifier? = (bgTaskIdList[0] as? UIBackgroundTaskIdentifier)!
                print("ending background task with id -\(UInt(bgTaskId!))")
                application.endBackgroundTask(bgTaskId!)
                bgTaskIdList.remove(at: 0)
            }

            if bgTaskIdList.count > 0 {
                print("kept background task id \(bgTaskIdList[0])")
            }

            if all {
                print("no more background tasks running")
                application.endBackgroundTask(masterTaskId)
                masterTaskId = UIBackgroundTaskInvalid
            }
            else {
                print("kept master background task id \(UInt(masterTaskId))")
            }
        }
    }
}
