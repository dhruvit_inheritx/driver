//
//  LocationSharedModel.swift
//  Driver
//
//  Created by Nirav Shah on 6/26/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit

class LocationSharedModel: NSObject {

    var timer: Timer?
    var delay10Seconds: Timer?
    var bgTask: BackgroundTaskManager?
    var myLocationArray = [Any]()

    static let shareModel = LocationSharedModel()
}
