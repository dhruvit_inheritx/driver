//
//  LocationTracker.swift
//  Driver
//
//  Created by Nirav Shah on 6/26/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationTrackerDelegate : class {
    func didFinishedWithUserCurrentLocation(location : CLLocation, lastLocation : CLLocationCoordinate2D?)
    func didFinishedUpdatingNewHeading(degree : CGFloat)
}

class LocationTracker: NSObject {

    var myLastLocation = CLLocationCoordinate2D()
    var myLastLocationAccuracy = CLLocationAccuracy()
    var shareModel: LocationSharedModel?
    var myLocation = CLLocationCoordinate2D()
    var myLocationAccuracy = CLLocationAccuracy()

    weak var delegate : LocationTrackerDelegate?

    var locationManager = CLLocationManager()
    static let sharedInstance = LocationTracker()

    // MARK: - Initalisation Methods

    override init() {
        super.init()

        // Get the share model and also initialize myLocationArray
        self.shareModel = LocationSharedModel.shareModel
        self.shareModel?.myLocationArray = NSMutableArray() as! [Any]

        // set up location manager
        self.onSetupLocationManager()

        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationEnterBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }

    // MARK: - Private Methods

    func onSetupLocationManager() {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.activityType = .automotiveNavigation
        self.locationManager.distanceFilter = kCLDistanceFilterNone

        if CLLocationManager .headingAvailable() {
            print("Available")
        }

        // show always authorization pop up if required
        self.locationManager.requestAlwaysAuthorization()

        // start updating location
        self.locationManager.startUpdatingLocation()
    }

    func applicationEnterBackground() {

        // set up location manager
        //self.onSetupLocationManager()

        // Use the BackgroundTaskManager to manage all the background Task
        self.shareModel?.bgTask = BackgroundTaskManager.sharedInstance
        self.shareModel?.bgTask?.beginNewBackgroundTask()
    }

    func restartLocationUpdates() {

        if (shareModel?.timer != nil) {
            DispatchQueue.main.async {
                self.shareModel?.timer?.invalidate()
                self.shareModel?.timer = nil
            }
        }

        // set up location manager
        //self.onSetupLocationManager()
    }

    func startLocationTracking() {

        if CLLocationManager.locationServicesEnabled() == false {
//            let servicesDisabledAlert = UIAlertView(title: "Location Services Disabled", message: "You currently have all location services for this device disabled", delegate: nil, cancelButtonTitle: "OK", otherButtonTitles: "")
//            servicesDisabledAlert.show()
            print("You currently have all location services for this device disabled")
        }
        else {
            let authStatus : CLAuthorizationStatus = CLLocationManager.authorizationStatus()
            if authStatus == .denied || authStatus == .restricted {
                print("Authorisation Failed!!!")
            }
            else {
                print("AuthorizationStatus authorized")

                 // show always authorization pop up if required
                self.locationManager.requestAlwaysAuthorization()

                 // start updating location
                self.locationManager.startUpdatingLocation()
            }
        }
    }

    func stopLocationTracking() {
        print("stopLocationTracking")

        if (shareModel?.timer != nil) {
            DispatchQueue.main.async {
                self.shareModel?.timer?.invalidate()
                self.shareModel?.timer = nil
            }
        }

        // stop location
        self.locationManager.stopUpdatingLocation()
    }
    
    func updateLocationToServer() {

    }

    func stopLocationDelayBy10Seconds() {
        self.locationManager.stopUpdatingLocation()
        print("locationManager stop Updating after 10 seconds")
    }
}

// MARK: - CLLocationManager Delegate Methods

extension LocationTracker : CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {

        // Use the true heading if it is valid.
        let direction: CLLocationDirection = newHeading.magneticHeading

        let radians = -direction / 180.0 * .pi

        //For Rotate Niddle

        let angle: CGFloat = CGFloat(Utility.sharedInstance.degreesToRadians(degrees: Double(radians)))

        self.delegate?.didFinishedUpdatingNewHeading(degree: angle)
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        let newLocation: CLLocation? = locations.last
        let theLocation: CLLocationCoordinate2D? = newLocation?.coordinate
        let theAccuracy: CLLocationAccuracy? = newLocation?.horizontalAccuracy
//        let locationAge: TimeInterval? = -(newLocation?.timestamp.timeIntervalSinceNow)! as Double

//        if locationAge! > Double(30.0) {
//            return
//        }

        // Select only valid location and also location with good accuracy

        if newLocation != nil && theAccuracy! > Double(0) && theAccuracy! < Double(2000) && (!(theLocation?.latitude == 0.0 && theLocation?.longitude == 0.0)) {

            /*
            self.shareModel?.myLocationArray.removeAll()

            var dict = [AnyHashable: Any]()
            dict["latitude"] = Int((theLocation?.latitude)!)
            dict["longitude"] = Int((theLocation?.longitude)!)
            dict["theAccuracy"] = Int(theAccuracy!)

            // Add the vallid location with good accuracy into an array
            // Every 1 minute, I will select the best location based on accuracy and send to server
            self.shareModel?.myLocationArray.append(dict)*/

            if newLocation?.coordinate.latitude != self.myLastLocation.latitude && newLocation?.coordinate.longitude != self.myLastLocation.longitude {

                // trigger user current location
                self.delegate?.didFinishedWithUserCurrentLocation(location: newLocation!,lastLocation: self.myLastLocation)

                // set location
                self.myLastLocation = theLocation!
                self.myLastLocationAccuracy = theAccuracy!
            }
        }

        // start location updates in background when user logged in

        if DataModel.sharedInstance.onCheckIsUserLogIn() {

            // If the timer still valid, return it (Will not run the code below)
            if (self.shareModel?.timer != nil) {
                return
            }

            self.shareModel?.bgTask = BackgroundTaskManager.sharedInstance
            self.shareModel?.bgTask?.beginNewBackgroundTask()

            // Restart the locationMaanger after 1 minute
            self.shareModel?.timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(self.restartLocationUpdates), userInfo: nil, repeats: false)

            // Will only stop the locationManager after 10 seconds, so that we can get some accurate locations
            // The location manager will only operate for 10 seconds to save battery
            if ((self.shareModel?.delay10Seconds) != nil) {
                self.shareModel?.delay10Seconds?.invalidate()
                self.shareModel?.delay10Seconds = nil
            }

            self.shareModel?.delay10Seconds = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.stopLocationDelayBy10Seconds), userInfo: nil, repeats: false)
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //        switch error.code {
        //        case kCLErrorNetwork:
        //            // general, network-related error
        //            var alert = UIAlertView(title: "Network Error", message: "Please check your network connection.", delegate: self as! UIAlertViewDelegate, cancelButtonTitle: "OK", otherButtonTitles: "")
        //            alert.show()
        //        case kCLErrorDenied:
        //            var alert = UIAlertView(title: "Enable Location Service", message: "You have to enable the Location Service to use this App. To enable, please go to Settings->Privacy->Location Services", delegate: self as! UIAlertViewDelegate, cancelButtonTitle: "OK", otherButtonTitles: "")
        //            alert.show()
        //        default:
        //            break
        //        }
    }

}
