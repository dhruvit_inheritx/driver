//
//  Extension+Controller.swift
//  Driver
//
//  Created by Nirav Shah on 6/26/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import Foundation
import UIKit
import KVNProgress
import Toast_Swift

@IBDesignable extension UIView {
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor.init(cgColor: color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
}

extension UIView
{
    // cooy view to another view
    func copyView() -> UIView?
    {
        return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self)) as? UIView
    }
}

extension Int {
    func getDistanceInKM() -> String {
        return String(format: "%d KM",self)
    }
    
    func getAmountInRupees() -> String {
        return String(format: "%d Rs.",self)
    }
    
    func getPercentage() -> String {
        return String(format: "%d",self) + "%"
    }
}

extension Double {
    
    func getDistanceInKM() -> String {
        return String(format: "%.2f KM",self)
    }
    
    func getAmountInRupees() -> String {
        return String(format: "%.2f Rs.",self)
    }
    
    func getPercentage() -> String {
        return String(format: "%.2f",self) + "%"
    }
    
    // get round value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
}

extension UIViewController {
    
    // shared application delegate
    var appDelegate:AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    // show alert controller
    func onShowAlertController(title : String?,message : String?) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertVC.addAction(alertAction)

        self.present(alertVC, animated: true, completion: nil)
    }
    
    // show toast message
    func showTost(message: String) {
        self.view.makeToast(message, duration: 3.0, position: .bottom)
    }
    
    // show progress view
    func onShowProgress()  {
        KVNProgress.show(withStatus: "Connecting...")
    }
    
    // dismiss progress view
    func onDissmissProgress() {
        DispatchQueue.main.async {
            KVNProgress.dismiss()
        }
    }
}

extension Dictionary {
    func getAllValues() -> [Any] {
        return self.flatMap(){ $0.1 }
    }
}

extension String {
    
    // get date from timestamp
    func getDateFromTimeStamp() -> String {
        let date = Date.init(timeIntervalSince1970: TimeInterval(self)!)
        return "\(date)"
    }
    
    // get Date from String
    func getDate() -> Date {
        
        if self != EMPTY_STRING {
            let dateFormat = "yyyy-MM-dd HH:mm:ss +zzzz"
            
            let dateString = self
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFormat
            dateFormatter.timeZone = TimeZone(identifier: "UTC")
            let dateFromString = dateFormatter.date(from: dateString)
            return dateFromString!
        }
        
        return Date()
    }
    
    // get Date String from String
    func getDateString(_ pstrFormat:String) -> String {
        
        if self != EMPTY_STRING {
            let dateFormat = "yyyy-MM-dd HH:mm:ss +zzzz"
            
            let dateString = self
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFormat
            dateFormatter.timeZone = TimeZone(identifier: "UTC")
            let dateFromString = dateFormatter.date(from: dateString)
            
            dateFormatter.dateFormat = pstrFormat
            dateFormatter.timeZone = TimeZone.autoupdatingCurrent
            let strDate = dateFormatter.string(from: dateFromString!)
            //        dateFromString = dateFormatter.date(from: strDate)
            
            return strDate
        }
        
        return EMPTY_STRING
    }
    
    // get Local Date from String
    func getLocalDate(_ pstrFormat : String) -> Date {
        
        let dateString = self
        let dateFormat = "yyyy-MM-dd HH:mm:ss +zzzz"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let date = dateFormatter.date(from: dateString)
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = pstrFormat
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        let stringFromDate = dateFormatter.string(from: date!)
        let dateFromString = dateFormatter.date(from: stringFromDate)
        
        return dateFromString!
    }
    
    // remove Special Chars From PhoneNumber String
    func removeSpecialCharsFromPhoneNumberString(str: String) -> String {
        let chars = Set("1234567890".characters)
        return String(str.characters.filter { chars.contains($0) })
    }
    
    // condense Whitespace
    func condenseWhitespace() -> String {
        let components = self.components(separatedBy: NSCharacterSet.whitespacesAndNewlines)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }
}
