//
//  HelpViewController.swift
//  Driver
//
//  Created by Dhruvit on 18/07/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit

enum HelpControllerType {
    case Help
    case About
    case Privacy
}

class HelpViewController: UIViewController {
    
    @IBOutlet weak var webViewHelp : UIWebView!
    
    var controllerType : HelpControllerType = HelpControllerType.Help
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.onShowProgress()
        
        print("Controller Type :- ",controllerType)
        
        var url = Bundle.main.url(forResource: KEY_HELP_HTML, withExtension: KEY_HTML)
        
        switch controllerType {
        case HelpControllerType.About:
            url = Bundle.main.url(forResource: KEY_ABOUT_US_HTML, withExtension: KEY_HTML)
        case HelpControllerType.Help:
            url = Bundle.main.url(forResource: KEY_HELP_HTML, withExtension: KEY_HTML)
        case HelpControllerType.Privacy:
            url = Bundle.main.url(forResource: KEY_PRIVACY_POLICY_HTML, withExtension: KEY_HTML)
        }
        
        let request = URLRequest.init(url: url!)
        
        webViewHelp.delegate = self
        webViewHelp.loadRequest(request)
    }
    
    @IBAction func btn_backTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
}

extension HelpViewController : UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.onDissmissProgress()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("error :- ",error.localizedDescription)
        self.onDissmissProgress()
    }
    
}
