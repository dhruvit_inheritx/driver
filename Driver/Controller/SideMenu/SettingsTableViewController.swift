//
//  SettingsTableViewController.swift
//  Driver
//
//  Created by Dhruvit on 20/07/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit

class SettingsCell : UITableViewCell {
    
    @IBOutlet weak var switchCell : UISwitch!
    
}

class SettingsTableViewController: UITableViewController {
    
    @IBOutlet weak var cellDrive : SettingsCell!
    @IBOutlet weak var cellPush : SettingsCell!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.allowsSelection = false
        cellDrive.switchCell.isOn = APPDELEGATE.objUserLocal.isDriverAvailable
        
        if let isOn = UserDefaults.standard.value(forKey: KEY_IS_PUSH_ON) {
            cellPush.switchCell.isOn = isOn as! Bool
        }
//        else {
//            cellPush.switchCell.isOn = true
//            UserDefaults.standard.set(true, forKey: KEY_IS_PUSH_ON)
//        }
    }
    
    @IBAction func changeDriverAvailability(sender: UISwitch) {
        print("changeDriverAvailability :- ",sender.isOn)
        UserAuth.onUpdateDriverAvailability(availability: sender.isOn)
    }
    
    @IBAction func changePushNotificationOnOff(sender: UISwitch) {
        print("PushNotification :- ",sender.isOn)
        UserDefaults.standard.set(sender.isOn, forKey: KEY_IS_PUSH_ON)
    }
    
    @IBAction func addPaymentMethod() {
        print("addPaymentMethod")
    }
    
    @IBAction func deleteAccount() {
        print("deleteAccount")
        
        let alert = UIAlertController(title: KALERT_APP_NAME, message: KALERT_CONFIRM_DELETE_ACCOUNT, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction.init(title: KEY_YES, style: .default, handler: { (action) in
            self.deleteDriverAccount()
        }))
        alert.addAction(UIAlertAction.init(title: KEY_NO, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteDriverAccount() {
        let vc = DashBoardViewController()
        vc.clearDriverDetail()
        
        UserAuth.onDeleteDriverAccount { (error) in
            if error == nil {
                self.showTost(message: KALERT_DELETE_ACCOUNT_SUCCESS)
            }
            else {
                self.showTost(message: KALERT_DELETE_ACCOUNT_ERROR)
            }
        }
    }
    
    @IBAction func shareApp() {
        print("shareApp")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE_ABOUT {
            let vc = segue.destination as! HelpViewController
            vc.controllerType = HelpControllerType.About
        }
        else if segue.identifier == SEGUE_PRIVACY_POLICY {
            let vc = segue.destination as! HelpViewController
            vc.controllerType = HelpControllerType.Privacy
        }
    }
    
}
