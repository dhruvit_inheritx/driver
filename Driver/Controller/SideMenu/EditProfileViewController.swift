//
//  EditProfileViewController.swift
//  Driver
//
//  Created by Dhruvit on 13/07/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {
    
    @IBOutlet weak var txtFirstName : UITextField!
    @IBOutlet weak var txtLastName : UITextField!
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtMobile : UITextField!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        txtFirstName.text = APPDELEGATE.objUserLocal.userFirstName
        txtLastName.text = APPDELEGATE.objUserLocal.userLastName
        txtEmail.text = APPDELEGATE.objUserLocal.userEmail
        txtMobile.text = APPDELEGATE.objUserLocal.userPhoneNumber
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UIButton Actions
    
    @IBAction func btn_backTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_updateTap(_ sender: Any) {
        self.view.endEditing(true)
        if isValueChange(){
            if isUpdatefieldValid(){
                
                let dictUserDetail = [KEY_MOBILE:txtMobile.text!, KEY_FIRSTNAME:txtFirstName.text!, KEY_LASTNAME:txtLastName.text!]
                UserAuth.onUpdateDriverProfileDetail(dictDetail: dictUserDetail)
                
                self.showTost(message:KALERT_UPDATE_PROFILE_SUCCESS)
            }
        }
    }
    
    // MARK: - Custom Function
    
    func isValueChange() -> Bool {
        
        if self.txtFirstName.text! != APPDELEGATE.objUserLocal.userFirstName || self.txtLastName.text! != APPDELEGATE.objUserLocal.userLastName || self.txtMobile.text! != APPDELEGATE.objUserLocal.userPhoneNumber {
            return true
        }
        self.showTost(message:KALERT_UPDATE_PROFILE_ALREADY)
        return false
    }
    
    func isUpdatefieldValid() -> Bool {
        if txtFirstName.text! == "" {
            self.showTost(message:KALERT_FILL_BLANK)
            return false
        }
        if txtLastName.text! == "" {
                        self.showTost(message:KALERT_FILL_BLANK)
            return false
        }
        if !Utility.sharedInstance.isValidPhone(strPhone: txtMobile.text!) {
            self.showTost(message:KALERT_VALID_PHONE_NUMBER)
            return false
        }
        return true
    }
    
}

extension EditProfileViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        var returnValue = true
        
        if self.txtFirstName.isFirstResponder {
            self.txtLastName.becomeFirstResponder()
        }
        else if self.txtLastName.isFirstResponder {
            self.txtMobile.becomeFirstResponder()
        }
        else {
            textField .resignFirstResponder()
            returnValue = false
        }
        
        return returnValue
    }
    
}
