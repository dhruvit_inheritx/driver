//
//  SideMenuViewController.swift
//  Driver
//
//  Created by Dhruvit on 18/07/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit

let KEY_SIDEMENU_CELL = "SideMenuCell"

class SideMenuCell : UITableViewCell {
    
    @IBOutlet weak var imgView : UIImageView!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var btnSelect : UIButton!
}

class SideMenuViewController: UIViewController {
    
    @IBOutlet weak var lbl_username : UILabel!
    @IBOutlet weak var tblViewMenu : UITableView!
    
    weak var delegate : SidemenuDelegate?
    
    let arrayTitle = [KEY_HOME,KEY_PAYMENT,KEY_HISTORY,KEY_NOTIFICATION,KEY_SETTINGS,KEY_HELP,KEY_LOGOUT]
    let arrayImages = ["icn_home", "icn_payment", "icn_history", "icn_notifications", "icn_settings", "icn_help", "icn_logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        lbl_username.text = APPDELEGATE.objUserLocal.userFullName
        
        tblViewMenu.delegate = self
        tblViewMenu.dataSource = self
        tblViewMenu.isScrollEnabled = false
        tblViewMenu.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_selection(_ sender:UIButton) {
        delegate?.sidemenuSelectedIndex(Index: sender.tag)
    }
}

extension SideMenuViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SideMenuCell = tableView.dequeueReusableCell(withIdentifier: KEY_SIDEMENU_CELL) as! SideMenuCell
        cell.lblTitle.text = arrayTitle[indexPath.row]
        cell.imgView.image = UIImage(named: arrayImages[indexPath.row])
        cell.btnSelect.tag = indexPath.row + 2
        
        return cell
    }
    
}
