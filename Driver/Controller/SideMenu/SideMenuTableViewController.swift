//
//  SideMenuTableViewController.swift
//  Driver
//
//  Created by Dhruvit on 12/07/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit

protocol SidemenuDelegate: class {
    func sidemenuSelectedIndex(Index:Int)
}

class SideMenuTableViewController: UITableViewController {
    
    @IBOutlet weak var lbl_username : UILabel!
    
    weak var delegate : SidemenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbl_username.text = APPDELEGATE.objUserLocal.userFullName
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    //MARK:- UIButton Action
    
    @IBAction func btn_selection(_ sender:UIButton){
        
        delegate?.sidemenuSelectedIndex(Index: sender.tag)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
