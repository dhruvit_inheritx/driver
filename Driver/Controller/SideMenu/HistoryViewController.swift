//
//  HistoryViewController.swift
//  Driver
//
//  Created by Dhruvit on 18/07/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit

let KEY_HISTORY_CELL = "HistoryCell"

class HistoryCell: UITableViewCell {
    
    @IBOutlet weak var imgViewMap : UIImageView!
    @IBOutlet weak var lblDayTime : UILabel!
    @IBOutlet weak var lblFare : UILabel!
    @IBOutlet weak var lblDistance : UILabel!
    @IBOutlet weak var btnSelect : UIButton!
    
}

class HistoryViewController: UIViewController {
    
    @IBOutlet weak var tblViewHistory : UITableView!
    
    var arrayRideDetails = [RideDetail]()
    var objRideDetail = RideDetail()
    
    var startValue : Double = 0
    var limitValue : Int = 10
    var isLoadMore : Bool = true
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblViewHistory.delegate = self
        tblViewHistory.dataSource = self
        tblViewHistory.estimatedRowHeight = 180
        tblViewHistory.rowHeight = UITableViewAutomaticDimension
        tblViewHistory.separatorStyle = .none
        tblViewHistory.tableFooterView = UIView()
        
        print(Utility.sharedInstance.getCurrentTimeStamp())
        
        self.onFetchRideHistory()
    }
    
    func onFetchRideHistory() {
        
        self.onShowProgress()
        
        if self.startValue == 0 {
            
            RideDetail.onFetchRideHistory(startValue: startValue, limitValue: limitValue, complition: { (success, response) in
                if success == true {
                    print("success")
                    self.arrayRideDetails.append(contentsOf: response as! [RideDetail])
                    self.startValue = self.arrayRideDetails.last!.timestamp
                    
                    if (response as! [RideDetail]).count < self.limitValue {
                        self.isLoadMore = false
                    }
                    
                    self.tblViewHistory.reloadData()
                    self.onDissmissProgress()
                }
                else {
                    self.showTost(message: response as! String)
                    self.onDissmissProgress()
                    self.isLoadMore = false
                }
                
            })
        }
        else {
            RideDetail.onFetchRideHistory(startValue: startValue, limitValue: limitValue, complition: { (success, response) in
                if success == true {
                    print("success")
                    self.arrayRideDetails.append(contentsOf: response as! [RideDetail])
                    self.startValue = self.arrayRideDetails.last!.timestamp
                    
                    if (response as! [RideDetail]).count < self.limitValue {
                        self.isLoadMore = false
                    }

                    self.tblViewHistory.reloadData()
                    self.onDissmissProgress()
                }
                else {
                    self.showTost(message: response as! String)
                    self.onDissmissProgress()
                    self.isLoadMore = false
                }
            })
        }
    }
    
    @IBAction func btn_cellTap(_ sender: UIButton) {
        self.objRideDetail = arrayRideDetails[sender.tag - 1]
    }
    
    @IBAction func btn_backTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE_RIDE_DETAIL {
            let rideDetailVC = segue.destination as! RideDetailViewController
            rideDetailVC.objRideDetail = self.objRideDetail
        }
    }
    
}

extension HistoryViewController : UITableViewDataSource, UITableViewDelegate {
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return tableView.estimatedRowHeight
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayRideDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : HistoryCell = tableView.dequeueReusableCell(withIdentifier: KEY_HISTORY_CELL) as! HistoryCell
        
//        let strTimeStamp = Utility.sharedInstance.getCurrentTimeStamp()
//        let timeInterval = String(arrayRideDetails[indexPath.row].payment_type)
//        let dateStr = timeInterval.getDateFromTimeStamp()
//        print(dateStr)
        
        if arrayRideDetails[indexPath.row].drop_time != EMPTY_STRING {
            
            print("drop_time:- ",arrayRideDetails[indexPath.row].drop_time)
            let dateOfRide:String = arrayRideDetails[indexPath.row].drop_time.getDateString("dd/MM/yyyy")
            let timeOfRide:String = arrayRideDetails[indexPath.row].drop_time.getDateString("HH:mm")
            
            if dateOfRide != EMPTY_STRING  && timeOfRide != EMPTY_STRING {
                cell.lblDayTime.text = "\(dateOfRide) at \(timeOfRide)"
            }
            else {
                cell.lblDayTime.text = "Ride not completed."
            }
        }
        else {
            cell.lblDayTime.text = "Ride not completed."
        }
        
        cell.imgViewMap.image = UIImage(named:"ride_history_map")
        cell.lblFare.text = arrayRideDetails[indexPath.row].fare
        cell.lblDistance.text = arrayRideDetails[indexPath.row].distance
        cell.btnSelect.tag = indexPath.row + 1
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == arrayRideDetails.count - 1 && isLoadMore == true {
            self.onFetchRideHistory()
        }
        
    }
    
}
