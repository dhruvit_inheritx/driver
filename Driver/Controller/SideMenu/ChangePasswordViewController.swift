//
//  ChangePasswordViewController.swift
//  Driver
//
//  Created by Dhruvit on 17/07/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit
import Foundation
import FirebaseAuth

class ChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var txtOldPass : UITextField!
    @IBOutlet weak var txtNewPass : UITextField!
    @IBOutlet weak var txtConfirmPass : UITextField!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_backTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmitTapped(sender: Any) {
        
        if isChangePasswordfieldsValid() {
            UserAuth.onUpdatePassword(password: txtNewPass.text!, completion: { (error) in
                
                if error == nil {
                    try! KeychainConfiguration.passwordItem().savePassword(self.txtNewPass.text!)
                    self.txtOldPass.text = ""
                    self.txtNewPass.text = ""
                    self.txtConfirmPass.text = ""
                    self.showTost(message:KALERT_CHANGE_PASSWORD_SUCCESS)
                }
                else {
                    self.showTost(message:(error?.localizedDescription)!)
                }
                
            })
        }
    }
    
    // MARK: - Custom Function
    
    func isChangePasswordfieldsValid() -> Bool {
        
        let userpwd = try! KeychainConfiguration.passwordItem().readPassword()
        
        if userpwd as String? != txtOldPass.text {
            self.showTost(message:KALERT_OLD_PASSWORD_VALID)
            return false
        }
        
        if  Utility.sharedInstance.isPasswordValid(password: txtNewPass.text!) {
            self.showTost(message:KALERT_VALID_PASSWORD)
            return false
        }
        
        if txtNewPass.text != txtConfirmPass.text {
            self.showTost(message:KALERT_MATCH_PASSWORD)
            return false
        }
        return true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

extension ChangePasswordViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        var returnValue = true
        
        if self.txtOldPass.isFirstResponder {
            self.txtNewPass.becomeFirstResponder()
        }
        else if self.txtNewPass.isFirstResponder {
            self.txtConfirmPass.becomeFirstResponder()
        }
        else {
            textField .resignFirstResponder()
            returnValue = false
        }
        
        return returnValue
    }
    
}
