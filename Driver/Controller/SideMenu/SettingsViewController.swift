//
//  SettingsViewController.swift
//  Driver
//
//  Created by Dhruvit on 18/07/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
//        tblViewSettings.separatorStyle = .none
//        tblViewSettings.tableFooterView = UIView()
    }
    
    @IBAction func btn_backTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
