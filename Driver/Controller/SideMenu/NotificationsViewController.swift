//
//  NotificationsViewController.swift
//  Driver
//
//  Created by Dhruvit on 18/07/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit

let KEY_NOTIFICATION_CELL = "NotificationCell"

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var lblRideTitle : UILabel!
    @IBOutlet weak var lblFare : UILabel!
    @IBOutlet weak var lblDestination : UILabel!
    @IBOutlet weak var lblSource : UILabel!
    @IBOutlet weak var btnSelect : UIButton!
    
}

class NotificationsViewController: UIViewController {
    
    @IBOutlet weak var tblViewNotification : UITableView!
    
    let arrayRideTitle = ["Ride_01", "Ride_02", "Ride_03", "Ride_04", "Ride_05", "Ride_06", "Ride_07"]
    let arrayFare = ["$25.20", "$10.53", "$09.24", "$31.38", "$51.29", "$18.24", "$45.23"]
    let arrayDestination = ["Surat", "Diu", "Anand", "Ahmedabad", "Rajkot", "Baroda", "Bhavnagar"]
    let arraySource = ["Ahmedabad", "Bhavnagar", "Baroda", "Rajkot", "Surat", "Anand", "Diu"]
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        tblViewNotification.delegate = self
        tblViewNotification.dataSource = self
        tblViewNotification.separatorStyle = .none
        tblViewNotification.tableFooterView = UIView()
    }
    
    @IBAction func btn_backTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension NotificationsViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayRideTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : NotificationCell = tableView.dequeueReusableCell(withIdentifier: KEY_NOTIFICATION_CELL) as! NotificationCell
        
        cell.lblRideTitle.text = arrayRideTitle[indexPath.row]
        cell.lblFare.text = arrayFare[indexPath.row]
        cell.lblDestination.text = arrayDestination[indexPath.row]
        cell.lblSource.text = arraySource[indexPath.row]
        cell.btnSelect.tag = indexPath.row
        
        return cell
    }
    
}
