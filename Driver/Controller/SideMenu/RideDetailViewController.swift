//
//  RideDetailViewController.swift
//  Driver
//
//  Created by Dhruvit on 03/08/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit

class RideDetailViewController: UIViewController {
    
    @IBOutlet weak var lblCustomerName : UILabel!
    @IBOutlet weak var lblFare : UILabel!
    @IBOutlet weak var lblDistance : UILabel!
    @IBOutlet weak var lblPricePerKm : UILabel!
    @IBOutlet weak var lblTax : UILabel!
    @IBOutlet weak var lblDiscount : UILabel!
    @IBOutlet weak var lblPaymentType : UILabel!
    @IBOutlet weak var lblSource : UILabel!
    @IBOutlet weak var lblDestination : UILabel!
    @IBOutlet weak var lblPickUpTime : UILabel!
    @IBOutlet weak var lblDropTime : UILabel!
    
    var objRideDetail = RideDetail()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        RideDetail.onFetchCustomerDetail(customerID: objRideDetail.customer_id!) { (success, response) in
            
            if success {
                print(response as! [String:Any])
                let dictCustomerDetail = response as! [String:String]
                self.lblCustomerName.text = dictCustomerDetail["name"]! + " " + dictCustomerDetail["lastname"]!
             }
            else {
                print(response as! String)
            }
            
        }
        
        lblCustomerName.text = "-"
        lblFare.text = objRideDetail.fare
        lblDistance.text = objRideDetail.distance
        lblPricePerKm.text = objRideDetail.priceperkm
        lblTax.text = objRideDetail.tax
        lblDiscount.text = objRideDetail.discount
        lblPaymentType.text = String(format:"%.0f",objRideDetail.payment_type)
        lblSource.text = objRideDetail.source_address
        lblDestination.text = objRideDetail.destination_address
//        lblPickUpTime.text = objRideDetail.pickup_time.getDate("dd-MM-yyyy hh:mm a")
//        lblDropTime.text = objRideDetail.drop_time.getDate("dd-MM-yyyy hh:mm a")
        
        let dateOfPickUp:String = objRideDetail.pickup_time.getDateString("dd/MM/yyyy")
        let timeOfPickUp:String = objRideDetail.pickup_time.getDateString("HH:mm")
        
        if dateOfPickUp != EMPTY_STRING  && timeOfPickUp != EMPTY_STRING {
            lblPickUpTime.text = "\(dateOfPickUp) at \(timeOfPickUp)"
        }
        else {
            lblPickUpTime.text = "Ride not completed."
        }
        
        let dateOfDrop:String = objRideDetail.drop_time.getDateString("dd/MM/yyyy")
        let timeOfDrop:String = objRideDetail.drop_time.getDateString("HH:mm")
        
        if dateOfDrop != EMPTY_STRING  && timeOfDrop != EMPTY_STRING {
            lblDropTime.text = "\(dateOfDrop) at \(timeOfDrop)"
            
            var averageSpeed = 0.0
            let dropTime = objRideDetail.drop_time.getDate()
            let pickUpTime = objRideDetail.pickup_time.getDate()
            var travellingTime = dropTime.timeIntervalSince(pickUpTime)
            travellingTime = travellingTime/60 // convert to "Minutes"
            travellingTime = travellingTime/60 // convert to "Hours"
            
            let distance : String! = objRideDetail.distance.replacingOccurrences(of: " KM", with: "")
            averageSpeed = Double(distance)! / travellingTime
            print("averageSpeed :- ", averageSpeed)
        }
        else {
            lblDropTime.text = "Ride not completed."
        }
        
    }
    
    @IBAction func btn_closeTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
