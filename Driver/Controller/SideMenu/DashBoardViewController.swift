//
//  DashBoardViewController.swift
//  Driver
//
//  Created by Nirav Shah on 6/22/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps
import SideMenu
import Firebase

class DashBoardViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var viewMap : UIView!
    @IBOutlet weak var viewBottom : UIView!
    
    @IBOutlet weak var btnRideDetail : UIButton!
    
    @IBOutlet weak var mapViewBottomConstraint : NSLayoutConstraint!
    
    // MARK: - properties
    var mapView : GMSMapView? = nil
    var polyline: GMSPolyline? = nil
    
    var customDriverAnnotation : CustomAnnotation? = nil
    var customerLocation : CustomAnnotation? = nil
    
    var imageViewDriver : UIImageView?
    
    var requestRide : RideRequest?
    var ridePopup : StartRidePopup?
    var completeRide : CompleteRide?
    var rideDetailPopup : RideStatusView?
    var destinationCoordinate : CLLocationCoordinate2D? = nil
    var sourceCoordinate : CLLocationCoordinate2D? = nil
    var tempIncrement : UInt = 0
    var isDriverMoving = false
    var isRideStart = false
    
    var startDate : Date?
    var endDate : Date?
    
    var reqTimer = Timer()
    var rideTimer = Timer()
    
    var arrayCoordinate : [CLLocationCoordinate2D] = []
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // Fetch Current User Details
        
//        if UserDefaults.standard.value(forKey: KEY_USERDATA) != nil {
//            APPDELEGATE.objUserLocal = UserLocal(dictionary: UserDefaults.standard.value(forKey: KEY_USERDATA) as! [String:Any])
//        }
//        else {
            UserAuth.onSaveUserDataInLocal()
//        }
        
        // Add Side Menu
        
        let appScreenRect = UIApplication.shared.keyWindow?.bounds ?? UIWindow().bounds
        
        SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the view controller it displays!
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        SideMenuManager.menuAnimationBackgroundColor = UIColor.clear
        SideMenuManager.menuPresentMode = .menuSlideIn
        SideMenuManager.menuWidth = max(round(min((appScreenRect.width), (appScreenRect.height)) * 0.80), 240)
        
        // get notifcation of location updates
        LocationTracker.sharedInstance.delegate = self
        
        //        self.destinationCoordinate = CLLocationCoordinate2D(latitude: 23.042435, longitude: 72.515391)
        self.destinationCoordinate = LocationTracker.sharedInstance.myLastLocation
        self.sourceCoordinate = LocationTracker.sharedInstance.myLastLocation
        
        // Add Google MapView
        self.onAddGoogleMapView()
        
        // add observer for ride status check
        RideRequestDetail.onAddObserverForRideStatusChange()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onShowRequestView), name: NSNotification.Name(rawValue: kRideRequestInitialStateKey), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(drawPathFromDriverToSourceAddress), name: NSNotification.Name(rawValue: kRideRequestConfirmationStateKey), object: nil)
        
        self.viewBottom.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.appDelegate.onShowAndHideNavigationBar(isHidden: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAction Methods
    
    func btnLogout_click() {
        
        self.clearDriverDetail()
        
        try! Auth.auth().signOut()
        
        if Auth.auth().currentUser == nil {
            
        }
    }
    
    func clearDriverDetail() {
        rideTimer.invalidate()
        UserAuth.onUpdateDriverAvailability(availability: false)
        try! KeychainConfiguration.passwordItem().deleteItem()
        DataModel.sharedInstance.onSetUserEmailAddress(userEmailAddress: "")
        DataModel.sharedInstance.onSetUserLogIn(value: false)
        APPDELEGATE.onSetRootViewController()
        UserDefaults.standard.removeObject(forKey: KEY_USERDATA)
        UserDefaults.standard.removeObject(forKey: KEY_IS_PUSH_ON)
        
        APPDELEGATE.objUserLocal = nil
        APPDELEGATE.objRideRequestDetail = nil
        APPDELEGATE.dictRideDetail = ["":""]
        APPDELEGATE.strRideID = ""
    }
    
    // Show Ride Detail Popup
    @IBAction func btnRideDetail_click(_ sender: UIButton) {
        
        if sender.title(for: UIControlState.normal) == KEY_STOP_RIDE {
            
            let alertController = UIAlertController(title: KALERT_APP_NAME, message: KALERT_CANCEL_RIDE, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: { (action) in
                
                self.onChangeRideStatus(isPendingRequest: RideRequestStatus.DRIVER_REJECT)
                self.onChangeDriverAvilability(availability: true)
                self.onRemovePolylineView()
                self.rideTimer.invalidate()
                self.viewBottom.isHidden = true
            }))
            alertController.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.default, handler:nil))
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            
            // find Distance between two location
            var coordinateFirst = arrayCoordinate.first
            var totalMeters = 0.0
            
            for coordinateTemp in arrayCoordinate {
                
                let distanceFound = Utility.sharedInstance.onFindDistanceInMeterBetween(toCoordinate: coordinateFirst!, fromCoordinate: coordinateTemp)
                
                coordinateFirst = coordinateTemp
                totalMeters += distanceFound
            }
            
            // set distance in string
            let distanceInKM : Double = totalMeters / 1000.0
            let strDistaceKM =  distanceInKM.getDistanceInKM()
            
            // set Total Amount
            let totalAmount = Double(ONE_KM_AMOUNT) * distanceInKM
            
            // discount amount
            let discountAmount = totalAmount / 10.0
            let strDiscountAmount = discountAmount.getAmountInRupees()
            
            // tax amount
            let taxableAmount = totalAmount - discountAmount
            let taxAmount = taxableAmount / 10.0
            
            // set payable amount
            let payableAmount = taxableAmount + taxAmount
            var strPayableAmount = MIN_FARE_AMOUNT.getAmountInRupees()
            
            if payableAmount > Double(MIN_FARE_AMOUNT) {
                strPayableAmount = payableAmount.getAmountInRupees()
            }
            
            // per km fare
            let strPricePerKM = ONE_KM_AMOUNT.getAmountInRupees()
            
            let nibRequest = Bundle.main.loadNibNamed("RideStatusView", owner: self, options: nil)! as NSArray
            
            self.rideDetailPopup = nibRequest.object(at: 0) as? RideStatusView
            self.rideDetailPopup?.delegateRideDetail = self
            self.rideDetailPopup?.translatesAutoresizingMaskIntoConstraints = false
            
            self.rideDetailPopup?.lblFare.text = strPayableAmount
            self.rideDetailPopup?.lblDistance.text = strDistaceKM
            self.rideDetailPopup?.lblPricePerKm.text = strPricePerKM
            self.rideDetailPopup?.lblTax.text = TAX_PERCENTAGE.getPercentage()
            self.rideDetailPopup?.lblDiscount.text = strDiscountAmount
            self.rideDetailPopup?.lblSource.text = APPDELEGATE.objRideRequestDetail.sourcePlaceName
            self.rideDetailPopup?.lblDestination.text = APPDELEGATE.objRideRequestDetail.destPlacename
            
            // add sub view
            self.view.addSubview(self.rideDetailPopup!)
            
            let views = ["rideDetailPopup" : self.rideDetailPopup!, "viewSelf" : self.view] as [String : Any]
            
            let constraintHorizontal = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0.0-[rideDetailPopup]-0.0-|", options: [], metrics: nil, views: views)
            self.view.addConstraints(constraintHorizontal)
            
            let constraintVertical = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0.0-[rideDetailPopup]-0.0-|", options: [], metrics: nil, views: views)
            self.view.addConstraints(constraintVertical)
        }
        
    }
    
    @IBAction func btnMenu_click(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE_SIDEMENU, sender: nil)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SEGUE_SIDEMENU {
            
            if let nav = segue.destination as? UINavigationController, let classBVC = nav.topViewController as? SideMenuViewController {
                classBVC.delegate = self
            }
        }
    }
}

private extension DashBoardViewController {
    
    // add and confirure map view
    func onAddGoogleMapView() {
        let camera = GMSCameraPosition.camera(withLatitude: 23.04108, longitude: 72.513508, zoom: 18.0)
        
        let rectFrame = CGRect(x: 0.0, y: 0.0, width: self.view.bounds.size.width, height: self.view.bounds.size.height)
        self.mapView = GMSMapView.map(withFrame: rectFrame, camera: camera)
        self.mapView?.delegate = self
        //self.mapView?.isMyLocationEnabled = true
        
        // add sub view
        self.viewMap.addSubview(self.mapView!)
        
        self.onAddDriverLocationOnMap(location: LocationTracker.sharedInstance.myLastLocation)
    }
    
    // remove polyline from map
    func onRemovePolylineView() {
        if self.polyline != nil {
            self.polyline?.map = nil
        }
    }
    
    // zoom at user location
    func onZoomMapAtUserLocation(location: CLLocationCoordinate2D) {
        // show user current location as annotation
        self.mapView?.camera = GMSCameraPosition.camera(withTarget: location, zoom: 18.0)
    }
    
    // add driver location on map
    func onAddDriverLocationOnMap(location : CLLocationCoordinate2D) {
        if self.customDriverAnnotation == nil {
            
            // set marker view
            let house = UIImage(named: "car_small")!.withRenderingMode(.alwaysOriginal)
            let markerView = UIImageView(image: house)
            self.imageViewDriver = markerView
            
            self.customDriverAnnotation = self.onCreateAnnotation(withTitle: "I'm Driver", subTitle: "Location of Driver", location: location)
            self.customDriverAnnotation?.iconView = self.imageViewDriver
            self.customDriverAnnotation?.appearAnimation = .pop
            self.customDriverAnnotation?.isFlat = true
            self.customDriverAnnotation?.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            self.customDriverAnnotation?.tracksViewChanges = true
            self.customDriverAnnotation?.map = self.mapView
        }
        else {
            self.customDriverAnnotation?.position = location
        }
        
        // zoom the driver location
        self.onZoomMapAtUserLocation(location: location)
    }
    
    // create annotation
    func onCreateAnnotation(withTitle: String, subTitle: String?, location: CLLocationCoordinate2D) -> CustomAnnotation {
        let customAnnotation = CustomAnnotation(title: withTitle, locationName: subTitle!, coordinate: location)
        return customAnnotation
    }
    
    // start move driver location
    func onStartMoveDriverLocation(location : CLLocationCoordinate2D, lastLocation : CLLocationCoordinate2D?) {
        
        if self.customDriverAnnotation == nil {
            self.onAddDriverLocationOnMap(location: location)
        }
        else {
            
            // Keep Rotation Short
            CATransaction.begin()
            CATransaction.setAnimationDuration(0.2)
            
            let bearingOfDriver = Utility.sharedInstance.getBearing(toPoint: location, fromPoint: lastLocation!)
            self.customDriverAnnotation?.rotation = bearingOfDriver
            
            CATransaction.commit()
            
            // Movement
            CATransaction.begin()
            CATransaction.setAnimationDuration(5)
            self.customDriverAnnotation?.position = location
            
            // zoom the driver location
            let camera = GMSCameraUpdate.setTarget(location)
            self.mapView?.animate(with: camera)
            
            CATransaction.commit()
        }
    }
    
    // move driver location manually
    func onMoveDriverLocationManually(coordinateArray : [CLLocationCoordinate2D]) {
        
        if #available(iOS 10.0, *) {
            rideTimer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: true) { (timer) in
                
                if coordinateArray.count == Int(self.tempIncrement) {
                    timer.invalidate()
                    return
                }
                
                let coordinateTemp = coordinateArray[Int(self.tempIncrement)]
                let location = CLLocation.init(coordinate: coordinateTemp, altitude: 20, horizontalAccuracy: 20, verticalAccuracy: 20, timestamp: Date(timeIntervalSince1970: 0.0))
                
                let cllLocation = NSMutableArray.init(object: location)
                
                LocationTracker.sharedInstance.locationManager.delegate?.locationManager!(LocationTracker.sharedInstance.locationManager, didUpdateLocations: cllLocation as! [CLLocation])
                
                self.tempIncrement += 1
                
            }
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    // draw path from source to destination
    func onDrawRoutePathSourceToDestinationOnMap(source: CLLocationCoordinate2D, destination: CLLocationCoordinate2D) {
        
        self.onShowProgress()
        
        let origin = "\(source.latitude),\(source.longitude)"
        let destination = "\(destination.latitude),\(destination.longitude)"
        
        let strUrl = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(Constant.GoogleAPIKey.APIKey)"
        
        let urlPath = URL.init(string: strUrl)
        let urlRequest = URLRequest.init(url: urlPath!)
        
        let dataTask = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            self.onDissmissProgress()
            
            if error == nil {
                
                do {
                    let jsonDict = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! NSDictionary
                    //                    print(jsonDict)
                    
                    let routesArray = jsonDict["routes"] as! NSArray
                    
                    if ((routesArray as AnyObject).count)! > 0 {
                        var routeDict: [AnyHashable: Any]? = (routesArray[0] as? [AnyHashable: Any])
                        var routeOverviewPolyline: [AnyHashable: Any]? = (routeDict?["overview_polyline"] as? [AnyHashable: Any])
                        let points: String? = (routeOverviewPolyline?["points"] as? String)
                        let path = GMSPath.init(fromEncodedPath: points!)
                        
                        var coordinateArray = [CLLocationCoordinate2D]()
                        self.tempIncrement = 0
                        
                        var intTemp = 0
                        while UInt(intTemp) < (path?.count())! {
                            coordinateArray.append((path?.coordinate(at: UInt(intTemp)))!)
                            intTemp += 1
                        }
                        
                        Utility.sharedInstance.onCalculateDistanceFrom(arrayCoordinate: coordinateArray, complition: { (distance) in
                            
                            if distance <= 30 && self.isRideStart == false {
                                // show Start ride popup
                                if self.isRideStart == false && self.isDriverMoving == true {
                                    self.onShowStartRidePopup()
                                }
                                
                                // add Start Ride request view
                                self.isDriverMoving = false
                            }
                        })
                        
                        DispatchQueue.main.async {
                            
                            // remove draw path if already drawed
                            self.onRemovePolylineView()
                            
                            self.polyline = GMSPolyline(path: path)
                            self.polyline?.strokeWidth = 5.0
                            self.polyline?.map = self.mapView
                            
                            self.onMoveDriverLocationManually(coordinateArray: coordinateArray)
                        }
                    }
                }
                catch _ {
                    print("Json failure")
                }
            }
        }
        
        dataTask.resume()
    }
    
    // change driver availability
    func onChangeDriverAvilability(availability : Bool) {
        UserAuth.onUpdateDriverAvailability(availability: availability)
    }
    
    // change ride status
    func onChangeRideStatus(isPendingRequest : RideRequestStatus) {
        RideRequestDetail.onUpdateRideStatus(isPendingRequest: isPendingRequest)
    }
    
    // show request view
    @objc func onShowRequestView() {
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(1000 * 4)) {
        let nibRequest = Bundle.main.loadNibNamed("RideRequest", owner: self, options: nil)! as NSArray
        
        self.requestRide = nibRequest.object(at: 0) as? RideRequest
        self.requestRide?.delegateRide = self
        self.requestRide?.translatesAutoresizingMaskIntoConstraints = false
        
        let strName : String! = APPDELEGATE.objRideRequestDetail.name
        self.requestRide?.lblTitle.text = "\"\(strName!)\" want to ride, do you want to accept ?"
        
        let coordinateOfDriver = LocationTracker.sharedInstance.myLastLocation
        let coordinateOfSource = CLLocationCoordinate2D(latitude: APPDELEGATE.objRideRequestDetail.sourceLat, longitude: APPDELEGATE.objRideRequestDetail.sourceLong)
        let coordinateOfDestination = CLLocationCoordinate2D(latitude: APPDELEGATE.objRideRequestDetail.destLat, longitude: APPDELEGATE.objRideRequestDetail.destLong)
        
        var distanceFromDriver = 0.0
        var distanceFromSource = 0.0
        
        Utility.sharedInstance.oncalculateDistanceBetween(source: coordinateOfDriver, destination: coordinateOfSource) { (distance) in
            
            distanceFromDriver = distance / 1000
            print("distanceFromDriver:- ",distanceFromDriver)
            
            Utility.sharedInstance.oncalculateDistanceBetween(source: coordinateOfSource, destination: coordinateOfDestination) { (distance) in
                
                distanceFromSource = distance / 1000
                print("distanceFromSource:- ",distanceFromSource)
                
                self.requestRide?.lblSourceName.text = APPDELEGATE.objRideRequestDetail.sourcePlaceName
                self.requestRide?.lblDestinationName.text = APPDELEGATE.objRideRequestDetail.destPlacename
                self.requestRide?.lblDistcnceFromDriver.text = String(format: "%.2f KM",distanceFromDriver)
                self.requestRide?.lblDistcnceFromSource.text = String(format: "%.2f KM",distanceFromSource)
                self.addRideRequestView()
            }
        }
    }
    
    func addRideRequestView() {
        // add sub view
        self.view.addSubview(self.requestRide!)
        
        let views = ["requestRide" : self.requestRide!, "viewSelf" : self.view] as [String : Any]
        
        let constraintHorizontal = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0.0-[requestRide]-0.0-|", options: [], metrics: nil, views: views)
        self.view.addConstraints(constraintHorizontal)
        
        let constraintVertical = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0.0-[requestRide]-0.0-|", options: [], metrics: nil, views: views)
        self.view.addConstraints(constraintVertical)
        
        // start timer of timeout
        self.requestRide?.onStartTimerOfDuration()
        //        }
    }
    
    // show start ride popup
    func onShowStartRidePopup() {
        DispatchQueue.main.async {
            let nibRequest = Bundle.main.loadNibNamed("StartRidePopup", owner: self, options: nil)! as NSArray
            
            self.ridePopup = nibRequest.object(at: 0) as? StartRidePopup
            self.ridePopup?.delegateStart = self
            self.ridePopup?.translatesAutoresizingMaskIntoConstraints = false
            
            self.ridePopup?.lblFullName.text = APPDELEGATE.objRideRequestDetail.name!
            self.ridePopup?.lblPhoneNumber.text = APPDELEGATE.objRideRequestDetail.mobile!
            self.ridePopup?.lblLocation.text = APPDELEGATE.objRideRequestDetail.sourcePlaceName!
            
            // add sub view
            self.view.addSubview(self.ridePopup!)
            
            let views = ["requestRide" : self.ridePopup!, "viewSelf" : self.view] as [String : Any]
            
            let constraintHorizontal = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0.0-[requestRide]-0.0-|", options: [], metrics: nil, views: views)
            self.view.addConstraints(constraintHorizontal)
            
            let constraintVertical = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0.0-[requestRide]-0.0-|", options: [], metrics: nil, views: views)
            self.view.addConstraints(constraintVertical)
        }
    }
    
    // show complete ride popup
    func onShowCompleteRidePopup() {
        
        self.isRideStart = false
        self.isDriverMoving = false
        
        rideTimer.invalidate()
        
        let nibRequest = Bundle.main.loadNibNamed("CompleteRide", owner: self, options: nil)! as NSArray
        
        self.completeRide = nibRequest.object(at: 0) as? CompleteRide
        self.completeRide?.delegateCompleteRide = self
        self.completeRide?.translatesAutoresizingMaskIntoConstraints = false
        
        // add sub view
        self.view.addSubview(self.completeRide!)
        
        let views = ["requestRide" : self.completeRide!, "viewSelf" : self.view] as [String : Any]
        
        let constraintHorizontal = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0.0-[requestRide]-0.0-|", options: [], metrics: nil, views: views)
        self.view.addConstraints(constraintHorizontal)
        
        let constraintVertical = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0.0-[requestRide]-0.0-|", options: [], metrics: nil, views: views)
        self.view.addConstraints(constraintVertical)
        
        // find Distance between two location
        var coordinateFirst = arrayCoordinate.first
        var totalMeters = 0.0
        //        var counter = 1
        
        for coordinateTemp in arrayCoordinate {
            
            let distanceFound = Utility.sharedInstance.onFindDistanceInMeterBetween(toCoordinate: coordinateFirst!, fromCoordinate: coordinateTemp)
            
            coordinateFirst = coordinateTemp
            totalMeters += distanceFound
            
            //            counter += 1
        }
        
        // set time
        let strTimeMin = String(format: "%d Mins",Utility.sharedInstance.minutes(from: self.startDate!, toDate: self.endDate!))
        
        // set distance in string
        let distanceInKM = totalMeters / 1000.0
        let strDistaceKM =  distanceInKM.getDistanceInKM()
        
        // set Total Amount
        let totalAmount = Double(ONE_KM_AMOUNT) * distanceInKM
        let strTotalAmount = totalAmount.getAmountInRupees()
        
        // discount amount
        let discountAmount = totalAmount / 10.0
        
        // tax amount
        let taxableAmount = totalAmount - discountAmount
        let taxAmount = taxableAmount / 10.0
        
        // set payable amount
        let payableAmount = taxableAmount + taxAmount
        var strPayableAmount = MIN_FARE_AMOUNT.getAmountInRupees()
        
        if payableAmount > Double(MIN_FARE_AMOUNT) {
            strPayableAmount = payableAmount.getAmountInRupees()
        }
        
        // per km fare
//        let strPricePerKM = ONE_KM_AMOUNT.getAmountInRupees()
        
        // set values to popup
        self.completeRide?.lblAmount.text = strTotalAmount
        self.completeRide?.lblPayment.text = strPayableAmount
        self.completeRide?.lblTax.text = TAX_PERCENTAGE.getPercentage()
        self.completeRide?.lblDiscount.text = DISCOUNT_PERCENTAGE.getPercentage()
        self.completeRide?.lblDistance.text = strDistaceKM
        self.completeRide?.lblTime.text = strTimeMin
        
        // Generate ride completion time
        let strRideComplitionTime : String = String(format: "%@", Date() as CVarArg)
//        let strTimeStamp : String = Utility.sharedInstance.getCurrentTimeStamp()
        let dictRideDetail = [KEY_DROP_TIME : strRideComplitionTime,
                              KEY_FARE : strPayableAmount,
                              KEY_DISTANCE : strDistaceKM,
                              KEY_PAYMENT_TYPE : EMPTY_STRING,
//                              KEY_TIMESTAMP : Int(strTimeStamp)!,
//                              KEY_CUSTOMERID_TIMESTAMP : "\(APPDELEGATE.objRideRequestDetail.customerid!)_\(strTimeStamp)",
//            KEY_DRIVERID_TIMESTAMP : "\(Auth.auth().currentUser!.uid)_\(strTimeStamp)"
            ] as [String : Any]
        
        RideDetail.onUpdateRideDetail(dictDetail: dictRideDetail)
        
        // remove records from array
        arrayCoordinate.removeAll()
        
        self.onChangeRideStatus(isPendingRequest: RideRequestStatus.COMPLETE)
        self.onChangeDriverAvilability(availability: true)
        
        self.viewBottom.isHidden = true
    }
    
    // remove start drive popup
    func onRemoveStartDrivePopup() {
        self.ridePopup?.removeFromSuperview()
        self.ridePopup = nil
    }
    
    // remove request view
    func onRemoveRequestView() {
        self.requestRide?.removeFromSuperview()
        self.requestRide = nil
    }
    
    // remove complete drive popup
    func onRemoveCompleteDrivePopup() {
        self.completeRide?.removeFromSuperview()
        self.completeRide = nil
    }
    
    // draw path from driver address to source address
    
    @objc func drawPathFromDriverToSourceAddress() {
        
        reqTimer.invalidate()
        
        // add anotation on GMSMapView
        self.customerLocation = self.onCreateAnnotation(withTitle: "I'm Here", subTitle: "I am user!!!", location: self.destinationCoordinate!)
        self.customerLocation?.map = self.mapView
        
        // capture time of driving
        self.startDate = Date()
        
        // move driver location
        self.isDriverMoving = true
        
        let driverLocationCoordinate = LocationTracker.sharedInstance.myLastLocation
        self.destinationCoordinate = CLLocationCoordinate2D(latitude: APPDELEGATE.objRideRequestDetail.sourceLat, longitude: APPDELEGATE.objRideRequestDetail.sourceLong)
        
        // drow path between coordinate
        self.onDrawRoutePathSourceToDestinationOnMap(source: driverLocationCoordinate, destination: self.destinationCoordinate!)
        
        //        // move driver location
        //        self.isDriverMoving = true
        
        self.onDissmissProgress()
        
        self.viewBottom.isHidden = false
    }
    
    // Customer did cancel ride request
    
    @objc func userDidCancelRideRequest() {
        self.onDissmissProgress()
        self.onChangeDriverAvilability(availability: true)
        self.onChangeRideStatus(isPendingRequest: RideRequestStatus.CUSTOMER_REJECT)
        self.onShowAlertController(title: "Alert", message: "\(APPDELEGATE.objRideRequestDetail.name!) has canceled the ride request.")
    }
}

// MARK: - GMSMapView Delegate Methods

extension DashBoardViewController : GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        //        UIView.animate(withDuration: 5.0, animations: { () -> Void in
        //            //self.londonView?.tintColor = .blue
        //        }, completion: {(finished) in
        //            // Stop tracking view changes to allow CPU to idle.
        //            //self.london?.tracksViewChanges = false
        //        })
    }
}

// MARK: - LocationTracker Delegate Methods

extension DashBoardViewController : LocationTrackerDelegate {
    
    func didFinishedUpdatingNewHeading(degree: CGFloat) {
        if self.customDriverAnnotation != nil {
            self.customDriverAnnotation?.rotation = CLLocationDegrees(degree)
        }
    }
    
    func didFinishedWithUserCurrentLocation(location: CLLocation,lastLocation : CLLocationCoordinate2D?) {
        
        let destiNationLocation = CLLocation(latitude: (self.destinationCoordinate?.latitude)!, longitude: (self.destinationCoordinate?.longitude)!)
        let distanceMeter = location.distance(from: destiNationLocation)
        
        print("Distance in Meter: \(Utility.sharedInstance.onFindDistanceInMeterBetween(toCoordinate: location.coordinate, fromCoordinate: self.destinationCoordinate!))")
        
        if isDriverMoving {
            self.onStartMoveDriverLocation(location: location.coordinate,lastLocation: lastLocation)
        }
        else {
            self.sourceCoordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            
            // add driver location on map
            self.onAddDriverLocationOnMap(location: location.coordinate)
        }
        
        if distanceMeter < 30.0 {
            // show Start ride popup
            if self.isRideStart == false && self.isDriverMoving == true {
                self.onShowStartRidePopup()
            }
            
            // add Start Ride request view
            self.isDriverMoving = false
        }
        
        // is ride start
        if self.isRideStart == true {
            // add location to array
            arrayCoordinate.append(location.coordinate)
        }
        
        // show Complete Ride Popup
        if distanceMeter < 30.0 && self.isRideStart == true {
            // capture time of driving
            self.endDate = Date()
            
            // show completion ride popup
            self.onShowCompleteRidePopup()
        }
        
        // update driver location on firebase
        UserAuth.onUpdateDriverLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
    }
}

// MARK: - PopupViews Delegate Methods

extension DashBoardViewController : RideRequestDelegate, StartRidePopupDelegate, CompleteRideDelegate, RideDetailDelegate {
    
    func didFinishedWithRideDetailAction(action: RideDetailAction) {
        
        switch action {
        case .continueRide:
            print("continueRide")
        case .stopRide:
            print("stopRide")
            
            let alertController = UIAlertController(title: KALERT_APP_NAME, message: KALERT_CANCEL_RIDE, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: { (action) in
                
                self.btnRideDetail.setTitle(KEY_STOP_RIDE, for: UIControlState.normal)
                
                // capture time of driving
                self.endDate = Date()
                
                // show completion ride popup
                self.onShowCompleteRidePopup()
            }))
            alertController.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.default, handler:nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    func didFinishedWithAction(action: RideAction) {
        
        // remove Request view
        self.onRemoveRequestView()
        
        switch action {
        case .accept:
            
            self.onShowProgress()
            
            // change driver avaliability
            self.onChangeDriverAvilability(availability: false)
            
            // change ride request status
            self.onChangeRideStatus(isPendingRequest: RideRequestStatus.DRIVER_ACCEPT)
            
            // Start timer for customer accept response
            reqTimer = Timer()
            reqTimer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(userDidCancelRideRequest), userInfo: nil, repeats: false)
            
        case .reject:
            print("nothing to do here!!!!")
            // change ride request status
            self.onChangeRideStatus(isPendingRequest: RideRequestStatus.DRIVER_REJECT)
        default:
            print("nothing to do here!!!!")
        }
    }
    
    func didFinishedWithStartRidePopupAction(action: StartAction) {
        
        // remove marker of customor
        self.customerLocation?.map = nil
        
        // remove Request view
        self.onRemoveStartDrivePopup()
        
        switch action {
        case .start:
            
            rideTimer.invalidate()
            self.tempIncrement = 0
            
            // change ride request status
            self.onChangeRideStatus(isPendingRequest: RideRequestStatus.START_TRIP)
            
            // capture time of driving
            self.startDate = Date()
            
            // move driver location
            self.isDriverMoving = true
            
            // start ride
            self.isRideStart = true
            
            self.sourceCoordinate = CLLocationCoordinate2D(latitude: APPDELEGATE.objRideRequestDetail.sourceLat, longitude: APPDELEGATE.objRideRequestDetail.sourceLong)
            self.destinationCoordinate = CLLocationCoordinate2D(latitude: APPDELEGATE.objRideRequestDetail.destLat, longitude: APPDELEGATE.objRideRequestDetail.destLong)
            
            // add location to array
            self.arrayCoordinate.append(self.sourceCoordinate!)
            
            // Draw Path
            self.onDrawRoutePathSourceToDestinationOnMap(source: self.sourceCoordinate!, destination: self.destinationCoordinate!)
            
            let saddrLat : String! = "\(self.sourceCoordinate!.latitude)"
            let saddrLong : String! = "\(self.sourceCoordinate!.longitude)"
            let daddrLat : String! = "\(self.destinationCoordinate!.latitude)"
            let daddrLong : String! = "\(self.destinationCoordinate!.longitude)"
            
            let alert = UIAlertController(title: KALERT_TITLE_GOOGLE_MAPS, message: KALERT_OPEN_GOOGLE_MAPS, preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (action) in
                
                // Redirect to "Google Maps" App for voice based navigation
                if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!) {
                    let strGoogleUrl = Utility.sharedInstance.getGoogleMapString(saddrLat:saddrLat, saddrLong:saddrLong, daddrLat:daddrLat, daddrLong: daddrLong)
                    UIApplication.shared.openURL(URL(string: strGoogleUrl)!)
                }
                else {
                    let strGoogleUrl = "http://maps.google.com/maps?saddr=\(saddrLat!),\(saddrLong!)&daddr=\(daddrLat!),\(daddrLong!)&zoom=14&views=traffic"
                    UIApplication.shared.openURL(URL(string: strGoogleUrl)!)
                }
            })
            
            let cancleAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil)
            
            alert.addAction(okAction)
            alert.addAction(cancleAction)
            self.present(alert, animated: true, completion: nil)
            
            self.viewBottom.isHidden = false
            btnRideDetail.setTitle(KEY_RIDE_DETAIL, for: UIControlState.normal)
            
            // Generate ride completion time
            let strRidePickUpTime : String = String(format: "%@", Date() as CVarArg)
//            let strTimeStamp : String = Utility.sharedInstance.getCurrentTimeStamp()
            
            let dictRideDetail = [KEY_PICKUP_TIME : strRidePickUpTime,
//                                  KEY_CUSTOMERID_TIMESTAMP : "\(APPDELEGATE.objRideRequestDetail.customerid!)_\(strTimeStamp)",
//                KEY_DRIVERID_TIMESTAMP : "\(Auth.auth().currentUser!.uid)_\(strTimeStamp)",
                KEY_FARE : MIN_FARE_AMOUNT.getAmountInRupees()]
            
            RideDetail.onUpdateRideDetail(dictDetail: dictRideDetail)
            
        default:
            print("Start Ride Cancel")
            
            self.viewBottom.isHidden = true
            btnRideDetail.setTitle(KEY_STOP_RIDE, for: UIControlState.normal)
            
            // change driver availability
            self.onChangeDriverAvilability(availability: true)
            
            // change ride request status
            self.onChangeRideStatus(isPendingRequest: RideRequestStatus.DRIVER_REJECT)
            
            // Remove Polyline
            self.onRemovePolylineView()
        }
    }
    
    func didFinishedWithCompleteRide() {
        
        // remove Complete Ride Popup
        self.onRemoveCompleteDrivePopup()
        
        // remove draw path if already drawed
        self.onRemovePolylineView()
        
        // change driver availability
        self.onChangeDriverAvilability(availability: true)
        
        self.viewBottom.isHidden = true
    }
}

// MARK: - SidemenuDelegate

extension DashBoardViewController : SidemenuDelegate {
    
    func sidemenuSelectedIndex(Index: Int) {
        
        print(Index)
        
        switch Index {
        case 1:
            // Edit Profile
            dismiss(animated: true, completion: nil)
            performSegue(withIdentifier: SEGUE_EDIT_PROFILE_SCREEN, sender: nil)
        case 2:
            // Home
            dismiss(animated: true, completion: nil)
        case 3:
            // Payment
            dismiss(animated: true, completion: nil)
            performSegue(withIdentifier: SEGUE_PAYMENT, sender: nil)
        case 4:
            // History
            dismiss(animated: true, completion: nil)
            performSegue(withIdentifier: SEGUE_HISTORY, sender: nil)
        case 5:
            // Notifications
            dismiss(animated: true, completion: nil)
            performSegue(withIdentifier: SEGUE_NOTIFICATIONS, sender: nil)
        case 6:
            // Settings
            dismiss(animated: true, completion: nil)
            performSegue(withIdentifier: SEGUE_SETTINGS, sender: nil)
        case 7:
            // Help
            dismiss(animated: true, completion: nil)
            performSegue(withIdentifier: SEGUE_HELP, sender: nil)
        case 8:
            // Log Out
            dismiss(animated: true, completion: nil)
            let alert = UIAlertController(title: KALERT_APP_NAME, message: KALERT_CONFIRM_LOGOUT, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: KEY_YES, style: .default, handler: { (action) in
                self.btnLogout_click()
            }))
            alert.addAction(UIAlertAction.init(title: KEY_NO, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            dismiss(animated: true, completion: nil)
        }
    }
    
}

