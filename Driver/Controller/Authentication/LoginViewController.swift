//
//  LoginViewController.swift
//  Driver
//
//  Created by Nirav Shah on 6/22/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        // fetch user current location
        self.appDelegate.onFetchUserLocation()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAction Methods
    
    @IBAction func btn_backTap(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHandleBackgroundTap(_ sender: Any) {
        self.txtEmail.resignFirstResponder()
        self.txtPassword.resignFirstResponder()
    }

    @IBAction func btnLogin_click(_ sender: Any) {

        let email = self.txtEmail.text
        let password = self.txtPassword.text

        if email == EMPTY_STRING || password == EMPTY_STRING {
            self.showTost(message: KALERT_FILL_BLANK)
        }
        else if !Utility.sharedInstance.isValidEmail(email: email) {
            self.showTost(message: KALERT_INVALID_EMAIL)
        }
        else if ReachabilityManager.shared.isNetworkAvailable == false {
            self.showTost(message: KALERT_INTERNET_ERROR)
        }
        else {

            self.onShowProgress()

            UserAuth.onSiginUser(withEmail: email!, password: password!, completion: { (user, error) in

                if error == nil {

                    UserAuth.onCheckIsUserDriver(completion: { (value) in

                        self.onDissmissProgress()

                        if value {

                            let latitude : Double? = LocationTracker.sharedInstance.myLastLocation.latitude
                            let longitude : Double? = LocationTracker.sharedInstance.myLastLocation.longitude
                            
                            // save user password in keychain
                            try! KeychainConfiguration.passwordItem().savePassword(password!)
                            
                            // get updated user profile
                            UserAuth.onSaveUserDataInLocal()
                            
                            // update user current location
                            UserAuth.onUpdateDriverLocation(latitude: latitude, longitude: longitude)
                            // update user availability
                            UserAuth.onUpdateDriverAvailability(availability: true)
                            
                            // store user email and login detail
                            DataModel.sharedInstance.onSetUserEmailAddress(userEmailAddress: email)
                            DataModel.sharedInstance.onSetUserLogIn(value: true)
                            
                            // change controller after success login
                            self.appDelegate.onSetRootViewController()
                        }
                        else {
                            self.showTost(message: KALERT_USER_DOSENT_EXISTS)
                        }
                    })

                }
                else {

                    self.onDissmissProgress()

                    if let errorTemp = error {
                        let errorCode = (errorTemp as NSError).code

                        if errorCode == 17011 {
                            self.showTost(message: KALERT_USER_DOSENT_EXISTS)
                        }
                        else if errorCode == 17009 {
                            self.showTost(message: KALERT_EMAIL_AND_PASSWORD_NOT_MATCH)
                        }
                        else {
                            self.showTost(message: KALERT_CONNECTION_ERROR)
                        }
                    }
                }
            })
        }
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
}

extension LoginViewController : UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        var returnValue = true

        if self.txtEmail.isFirstResponder {
            self.txtPassword.becomeFirstResponder()
        }
        else {
            textField .resignFirstResponder()
            returnValue = false
        }

        return returnValue
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {

    }

    func textFieldDidEndEditing(_ textField: UITextField) {

    }
}

