//
//  RegisterViewController.swift
//  Driver
//
//  Created by Nirav Shah on 6/22/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit
import FirebaseAuth

class RegisterViewController: UIViewController {

    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        // fetch user current location
        self.appDelegate.onFetchUserLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - IBAction Methods
    
    @IBAction func btn_backTap(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnRegister_click(_ sender: Any) {

        let firstName = self.txtFirstName.text
        let lastName = self.txtLastName.text
        let email = self.txtEmail.text
        let phoneNumber = self.txtPhoneNumber.text
        let password = self.txtPassword.text
        let confPassword = self.txtConfirmPassword.text

        if firstName == EMPTY_STRING || lastName == EMPTY_STRING || email == EMPTY_STRING || phoneNumber == EMPTY_STRING || password == EMPTY_STRING || confPassword == EMPTY_STRING {
            self.showTost(message: KALERT_FILL_BLANK)
        }
        else if !Utility.sharedInstance.isValidEmail(email: email) {
            self.showTost(message: KALERT_INVALID_EMAIL)
        }
        else if !Utility.sharedInstance.isValidPhone(strPhone: phoneNumber!) {
            self.showTost(message: KALERT_VALID_PHONE_NUMBER)
        }
        else if password != confPassword {
            self.showTost(message: KALERT_PASSWORD_AND_CONFIRM_PASSWORD_NOT_MATCH)
        }
        else {

            self.onShowProgress()

            let latitude = LocationTracker.sharedInstance.myLastLocation.latitude
            let longitude = LocationTracker.sharedInstance.myLastLocation.longitude

            // set user local property
            let userLocal = UserLocal()
            userLocal.initalisation(withEmail: email!, phoneNumber: phoneNumber!, firstName:  firstName!, lastName: lastName! , latitude: latitude, longitude: longitude)

            UserAuth.onCreateUser(withEmail: email!, password: password!, userLocal: userLocal, completion: { (user, error) in

                self.onDissmissProgress()

                if error == nil {
                    // save user password in keychain
                    try! KeychainConfiguration.passwordItem().savePassword(password!)
                    
                    // get updated user profile
                    UserAuth.onSaveUserDataInLocal()
                    
                    // update user availability
                    UserAuth.onUpdateDriverAvailability(availability: true)
                    
                    // store user email and login detail
                    DataModel.sharedInstance.onSetUserEmailAddress(userEmailAddress: email)
                    DataModel.sharedInstance.onSetUserLogIn(value: true)
                    
                    // change controller after success login
                    self.appDelegate.onSetRootViewController()
                }
            })
        }
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
}

extension RegisterViewController : UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        var returnValue = true

        if self.txtFirstName.isFirstResponder {
            self.txtLastName.becomeFirstResponder()
        }
        else if self.txtLastName.isFirstResponder {
            self.txtEmail.becomeFirstResponder()
        }
        else if self.txtEmail.isFirstResponder {
            self.txtPhoneNumber.becomeFirstResponder()
        }
        else if self.txtPhoneNumber.isFirstResponder {
            self.txtPassword.becomeFirstResponder()
        }
        else if self.txtPassword.isFirstResponder {
            self.txtConfirmPassword.becomeFirstResponder()
        }
        else {
            textField .resignFirstResponder()
            returnValue = false
        }

        return returnValue
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {

    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
}
