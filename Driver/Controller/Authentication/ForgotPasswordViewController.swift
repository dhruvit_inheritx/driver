//
//  ForgotPasswordViewController.swift
//  Driver
//
//  Created by Nirav Shah on 7/4/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var txtEmailForgot: UITextField!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - IBAction Methods
    
    @IBAction func btn_backTap(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnForgot_Password_click(_ sender: Any) {

        let email = self.txtEmailForgot.text

        if Utility.sharedInstance.isStringEmpty(string: email) {
            self.onShowAlertController(title: "", message: "Please enter email address")
        }
        else if !Utility.sharedInstance.isValidEmail(email: email) {
            self.onShowAlertController(title: "", message: "Please enter valid email address")
        }
        else if ReachabilityManager.shared.isNetworkAvailable == false {
            self.onShowAlertController(title: "", message: "Please check your internet connection")
        }
        else {
            
        }
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}
