//
//  RideStatusView.swift
//  Driver
//
//  Created by Dhruvit on 26/07/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit

enum RideDetailAction {
    case continueRide
    case stopRide
}

protocol RideDetailDelegate : class {
    func didFinishedWithRideDetailAction(action : RideDetailAction)
}

class RideStatusView: UIView {
    
    @IBOutlet weak var lblFare : UILabel!
    @IBOutlet weak var lblDistance : UILabel!
    @IBOutlet weak var lblPricePerKm : UILabel!
    @IBOutlet weak var lblTax : UILabel!
    @IBOutlet weak var lblDiscount : UILabel!
    @IBOutlet weak var lblSource : UILabel!
    @IBOutlet weak var lblDestination : UILabel!
    
//    @IBOutlet weak var viewMap : UIView!
    
    weak var delegateRideDetail : RideDetailDelegate?
    
    // MARK: IBAction Methods
    
    @IBAction private func btnContinue_click(_ sender: Any) {
        self.removeFromSuperview()
        self.delegateRideDetail?.didFinishedWithRideDetailAction(action: RideDetailAction.continueRide)
    }
    
    @IBAction private func btnStop_click(_ sender: Any) {
        self.removeFromSuperview()
        self.delegateRideDetail?.didFinishedWithRideDetailAction(action: RideDetailAction.stopRide)
    }
    
    @IBAction private func viewTapped(_ sender: Any) {
        self.removeFromSuperview()
        self.delegateRideDetail?.didFinishedWithRideDetailAction(action: RideDetailAction.continueRide)
    }
    
}
