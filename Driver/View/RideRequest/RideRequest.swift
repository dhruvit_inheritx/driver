//
//  RideRequest.swift
//  Driver
//
//  Created by Nirav Shah on 6/27/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit

enum RideAction : Int {
    case accept
    case reject
    case none
}

protocol RideRequestDelegate : class {
    func didFinishedWithAction(action : RideAction)
}

class RideRequest: UIView {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    
    @IBOutlet weak var lblSourceName : UILabel!
    @IBOutlet weak var lblDestinationName : UILabel!
    
    @IBOutlet weak var lblDistcnceFromSource : UILabel!
    @IBOutlet weak var lblDistcnceFromDriver : UILabel!
    
    var duration : UInt = 10
    var timerDuration : Timer?

    weak var delegateRide : RideRequestDelegate?
    var selectAction = RideAction(rawValue: 2)

    // initalisation
    func onStartTimerOfDuration() {
        if #available(iOS 10.0, *) {
            timerDuration = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
                self.onShowDurationTimeout()
            }
        } else {
            // Fallback on earlier versions
        }
    }

    private func onShowDurationTimeout() {
        //It will automatically rejected within 10 seconds
        self.duration -= 1
        self.lblDuration.text = "It will automatically rejected within \(self.duration) seconds"

        if self.duration == 0 {
            // send rejection of action
            self.delegateRide?.didFinishedWithAction(action: .reject)
            
            self.timerDuration?.invalidate()
            self.timerDuration = nil
        }
    }

    // MARK: IBAction Methods

    @IBAction private func btnAccept_click(_ sender: Any) {
        self.delegateRide?.didFinishedWithAction(action: .accept)
        self.timerDuration?.invalidate()
        self.timerDuration = nil
    }

    @IBAction private func btnReject_click(_ sender: Any) {
        self.delegateRide?.didFinishedWithAction(action: .reject)
        self.timerDuration?.invalidate()
        self.timerDuration = nil
    }

}
