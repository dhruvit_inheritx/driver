//
//  StartRidePopup.swift
//  Driver
//
//  Created by Nirav Shah on 6/29/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit

enum StartAction : Int {
    case start
    case cancel
}

protocol StartRidePopupDelegate : class {
    func didFinishedWithStartRidePopupAction(action : StartAction)
}

class StartRidePopup: UIView {

    @IBOutlet weak var imageCustomer: UIImageView!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblLocation: UILabel!

    weak var delegateStart : StartRidePopupDelegate?

    func initalsation() {
        self.imageCustomer.layer.cornerRadius = self.imageCustomer.bounds.size.height / 2.0
        self.imageCustomer.clipsToBounds = true
    }

    // MARK: - IBAction Methods

    @IBAction private func btnStartDriving_click(_ sender: Any) {
        self.delegateStart?.didFinishedWithStartRidePopupAction(action: .start)
    }

    @IBAction private func btnStopDriving_click(_ sender: Any) {
        self.delegateStart?.didFinishedWithStartRidePopupAction(action: .cancel)
    }
}
