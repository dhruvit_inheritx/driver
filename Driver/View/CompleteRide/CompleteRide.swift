//
//  CompleteRide.swift
//  Driver
//
//  Created by Nirav Shah on 6/30/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import UIKit

protocol CompleteRideDelegate : class {
    func didFinishedWithCompleteRide()
}

class CompleteRide: UIView {

    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblTax: UILabel!
    @IBOutlet weak var lblPayment: UILabel!

    weak var delegateCompleteRide : CompleteRideDelegate?

    // MARK:- IBAction Methods
    
    @IBAction func btnCompleteRide_click(_ sender: Any) {
        self.delegateCompleteRide?.didFinishedWithCompleteRide()
    }
}
