//
//  Constant.swift
//  Driver
//
//  Created by Nirav Shah on 6/27/17.
//  Copyright © 2017 Nirav Shah. All rights reserved.
//

import Foundation

let APPDELEGATE : AppDelegate = UIApplication.shared.delegate as! AppDelegate

// MARK: - Firebase Database Child Names
let KEY_DRIVER_DETAILS = "driver_details"
let KEY_RIDE_DETAILS = "ride_details"
let KEY_RIDE_MANAGER = "ride_manager"
let KEY_RIDE_NOTIFICATION = "ride_notification"

// MARK: - Side Menu Titles

let KEY_HOME = "Home"
let KEY_PAYMENT = "Payment"
let KEY_HISTORY = "History"
let KEY_NOTIFICATION = "Notification"
let KEY_SETTINGS = "Settings"
let KEY_HELP = "Help"
let KEY_LOGOUT = "Logout"

// HTML Files
let KEY_ABOUT_US_HTML = "about_us"
let KEY_HELP_HTML = "help"
let KEY_PRIVACY_POLICY_HTML = "privacy_policy"
let KEY_HTML = "html"

// MARK: - Ride Request Observer
let kRideRequestInitialStateKey = "RideRequestInitialStateKey"
let kRideRequestConfirmationStateKey = "RideRequestConfirmationStateKey"

// MARK: - Alert Messages
let KALERT_APP_NAME = "VdeliverU"
let KALERT_CONFIRM_LOGOUT = "Are you sure you want to logout?"
let KALERT_CONFIRM_DELETE_ACCOUNT = "are you sure you want to delete your account?"
let KALERT_USER_DOSENT_EXISTS = "Please check user doesn't exists."
let KALERT_UPDATE_PROFILE_SUCCESS = "Profile update successfully."
let KALERT_UPDATE_PROFILE_ALREADY = "Profile already updated."

let KALERT_INVALID_EMAIL = "Please enter valid email address"
let KALERT_EMAIL_AND_PASSWORD_NOT_MATCH = "Please check email and password doesn't match."
let KALERT_PASSWORD_AND_CONFIRM_PASSWORD_NOT_MATCH = "Please check password and confirm password doesn't match."
let KALERT_VALID_PHONE_NUMBER = "Please check phone number must be at least 10 characters"
let KALERT_OLD_PASSWORD_VALID = "Please enter correct old password."
let KALERT_CHANGE_PASSWORD_SUCCESS = "Password change successfully"
let KALERT_VALID_PASSWORD = "You can do better than that! Stronger password please (6 characters or more)"
let KALERT_MATCH_PASSWORD = "Password doesn't match."
let KALERT_DELETE_ACCOUNT_SUCCESS = "Account Deleted Successfully."
let KALERT_DELETE_ACCOUNT_ERROR = "Error Occured Deleting Account."

let KALERT_INTERNET_ERROR = "Please check your internet connection"
let KALERT_CONNECTION_ERROR = "We are not able to connect you right now, please try again letter!"

let KALERT_FILL_BLANK = "Please fill blank field(s)."
let KALERT_BLANK_EMAIL = "Please enter email address"
let KALERT_BLANK_PASSWORD = "Please enter password"
let KALERT_BLANK_CONFIRM_PASSWORD = "Please enter confirm password"
let KALERT_BLANK_FIRST_NAME = "Please enter first name"
let KALERT_BLANK_LAST_NAME = "Please enter last name"
let KALERT_BLANK_PHONE_NUMBER = "Please enter phone number"

let KALERT_CANCEL_RIDE = "Are you sure to stop ride?"
let KALERT_TITLE_GOOGLE_MAPS = "Redirect to Google Maps"
let KALERT_OPEN_GOOGLE_MAPS = "Are you sure to open 'Google Maps' for voice navigation."

//MARK : Segue Constant
let SEGUE_SIDEMENU = "segueSideMenu"
let SEGUE_EDIT_PROFILE_SCREEN = "segueEditProfile"
let SEGUE_PAYMENT = "seguePayment"
let SEGUE_HISTORY = "segueHistory"
let SEGUE_NOTIFICATIONS = "segueNotification"
let SEGUE_SETTINGS = "segueSettings"

let SEGUE_CHANGE_PASSWORD = "segueChangePassword"
let SEGUE_PRIVACY_POLICY = "seguePrivacyPolicy"
let SEGUE_ABOUT = "segueAbout"
let SEGUE_HELP = "segueHelp"
let SEGUE_RIDE_DETAIL = "segueRideDetail"

//MARK:- User's Constant Keys
let KEY_USER_ID = "user_id"
let KEY_EMAIL = "email"
let KEY_AVAILABLE = "available"
let KEY_FIRSTNAME = "firstname"
let KEY_LASTNAME = "lastname"
let KEY_MOBILE = "mobile"
let KEY_AGE = "age"
let KEY_CITY = "city"
let KEY_STATE = "state"
let KEY_LAST_SEEN = "lastseen"
let KEY_VECHICAL_DETAIL = "vehicle_details"
let KEY_LOCATION = "location"

let KEY_USERDATA = "userdata"
let KEY_LATITUDE = "latitude"
let KEY_LONGITUDE = "longitude"

// Button Title
let KEY_STOP_RIDE = "Stop Ride"
let KEY_RIDE_DETAIL = "Ride Detail"

// General
let EMPTY_STRING = ""
let KEY_IS_PUSH_ON = "isPushOn"
let KEY_YES = "Yes"
let KEY_NO = "No"

// Ride Details Keys
let KEY_RIDE_ID = "ride_id"
let KEY_CUSTOMER_ID = "customer_id"
let KEY_CUSTOMERID_TIMESTAMP = "customerid_timestamp"
let KEY_DRIVER_ID = "driver_id"
let KEY_DRIVERID_TIMESTAMP = "driverid_timestamp"
let KEY_SOURCE_ADDRESS = "source_address"
let KEY_DESTINATION_ADDRESS = "destination_address"
let KEY_SOURCE_LATG = "source_latg"
let KEY_SOURCE_LANG = "source_lang"
let KEY_DESTINATION_LATG = "destination_latg"
let KEY_DESTINATION_LANG = "destination_lang"
let KEY_FARE = "fare"
let KEY_DISTANCE = "distance"
let KEY_PRICE_PER_KM = "priceperkm"
let KEY_DISCOUNT = "discount"
let KEY_TAX = "tax"
let KEY_RIDE_CONFIRM_TIME = "ride_confirm_time"
let KEY_PICKUP_TIME = "pickup_time"
let KEY_DROP_TIME = "drop_time"
let KEY_PAYMENT_TYPE = "payment_type"
let KEY_TIMESTAMP = "timestamp"


// Strings
let STREMPTY = ""
let STRYEARAGO = "year ago"
let STRMONTHAGO = "month ago"
let STRWEEKAGO = "week ago"
let STRDAYSAGO = "days ago"
let STRDAYAGO = "day ago"
let STRHOURSAGO = "hours ago"
let STRHOURAGO = "hour ago"
let STRMINUTESAGO = "minutes ago"
let STRMINUTEAGO = "minute ago"
let STRFEWSECONDSAGO = "few seconds ago"

let ONE_KM_AMOUNT = 10
let DISCOUNT_PERCENTAGE = 10
let TAX_PERCENTAGE = 10
let MIN_FARE_AMOUNT = 50

//MARK: - Set Text Limit
struct MaxCharacter {
    static let FirstName = 35
    static let LastName = 35
    static let UserName = 20
    static let Hashtags = 20
    static let Phonenum = 10
    static let AboutMe = 140
    static let ZipCode = 6
}

//MARK:- Request Staus
enum RideRequestStatus : Int {
    case INITIAL = 0
    case DRIVER_ACCEPT = 1
    case DRIVER_REJECT = 2
    case CUSTOMER_REJECT = 3
    case CONFIRM = 4
    case START_TRIP = 5
    case COMPLETE = 6
}

struct Constant {
    
    struct GoogleAPIKey {
        static let APIKey = "AIzaSyC1nt8-h-I6ajek5dL85j1i0GYZCVGUvWw"
    }
    
}
